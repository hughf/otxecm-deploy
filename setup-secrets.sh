#!/bin/bash
WORKDIR=`pwd`
INFRADIR=${WORKDIR}/infra/k8s
echo Workdir is $WORKDIR
set -o allexport
source $WORKDIR/.env 
set +o allexport

# Connect kubectl to project
echo Connecting kubectl to $GCP_PROJECT ...
gcloud container clusters get-credentials ${GCP_CLUSTER} --zone ${GCP_ZONE} --project ${GCP_PROJECT}
# Create all required secrets
# Docker secret so we can log into OT registry
SECRET_NAME=hfc-docker
echo "Setting up Docker secret as ${DOCKER_REGISTRY} and host ${DOCKER_SERVER}"
kubectl create secret docker-registry hfc-docker --docker-server=${DOCKER_URL} --docker-username=${DOCKER_USER} --docker-password=${DOCKER_PWD} --docker-email=${DOCKER_USER}

# xecm-secret - used for storing TLS cert info
# THis name needs to be the same as the secret name
TLS_SECRET=xecm-secret
TLS_SECRET_CONF=${INFRADIR}/xecm-secret.yaml
echo "Creating xecm-secret for TLS data from file  ${TLS_SECRET_CONF}"
kubectl apply -f ${TLS_SECRET_CONF}

# otxecn-secrets - this secret set is for all the passwords we want to set 
# in our cluster. the OTDS and OTCS admin user passwords are set in this secret
# The name for our secret needs to be set in the existingSecret param in our <platform>.yaml
OT_SECRET=otxecm-secrets
OT_SECRET_CONF=${INFRADIR}/otcluster-secret.yaml
kubectl apply -f ${OT_SECRET_CONF}

