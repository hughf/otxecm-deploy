#!/bin/bash
OTDSKEY=123456789ABCDEFG
OTDSCRYPTKEY=`echo -n ${OTDSKEY} | base64 -w 0| base64 -w 0` 
#OTDSCRYPTKEY=Z2hkN2hyNDBkbWNGcVQ0TA==
WORKDIR=`pwd`
DEPLNAME=myotxecm
DEPLDIR=otxecm
PLATFORM=gcp.yaml
FILEARGS=" -f ${WORKDIR}/${DEPLDIR}/platforms/${PLATFORM} "
CMDARGS=""
CMDLINE=""

# Read through properties file. Skip empty lines and lines with comments
while IFS='=' read -r key value
do

    firstchar=${key:0:1}
    if [[ "$firstchar" == "#" || "$key" == "" ]] ; then
        #echo $key
        continue
    fi

    CMDARGS+=" --set ${key}=${value}"
done < ${WORKDIR}/helm-cmd-args.properties

# For the enablement of the container, parameters here. They will not work
# when pulled from the properties file. I think it has to do with the brackets.
# Note that name param below MUST NOT contain _ or caps. lower case letters and numbers and dashes only
# For simplicity, I set this to be the same as the name of the module I'm deploying.
CMDLINE+=" --set otcs.config.extensions.enabled=true"
CMDLINE+=" --set otcs.initContainers[0].name=ccmemlc"
CMDLINE+=" --set otcs.initContainers[0].image.source=gcr.io"
CMDLINE+=" --set otcs.initContainers[0].image.name=ccmemlc"
CMDLINE+=" --set otcs.initContainers[0].image.tag=23.4"

echo FILE ARGS:
echo $FILEARGS
echo COMMAND LINE ARGS:
echo $CMDARGS $CMDLINE


echo EXECUTING HELM....
echo "helm install ${DEPLNAME} ${WORKDIR}/${DEPLDIR} ${FILEARGS} ${CMDARGS} ${CMDLINE}"
helm install ${DEPLNAME} ${WORKDIR}/${DEPLDIR} ${FILEARGS} ${CMDARGS} ${CMDLINE}
