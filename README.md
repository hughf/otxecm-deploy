# OpenText Cloud Deployment Project 
This project was created to hold all the various scripts and configs and additional kubectl yamls I do
to try and get a Content Server deployment done.  This project focuses on deployment to Google Cloud.
My procedure also assumes you own a domain name for the purposes of this exercise. 

## How to use 

### Intialization in git
* Clone this project
* Create a .env file by copying .env-sample into your project and add it to your .gitignore 
* Install the following tools: 
    * Kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl/
    * Google Cloud SDK - https://cloud.google.com/sdk/install
    * Helm - https://helm.sh/docs/using_helm/#installing-helm
    * (optional) - VS Code (VSCode has a decent yaml editor)

This repository already has the helm charts for OTXECM 23.2. The main file that was modified was
otxecm/platforms/gcp.yaml 

### Preparation in GCP GUI
These steps can be executed manually, or using terraform. 

#### Manual GCP Artefact creation 
* Create a new GCP project - set this in GCP_PROJECT in your .env file;
* In your project create a new VPC network (I have been reusing the same one on each attempt);
* Create a new K8S cluster in your GCP project. Make it a standard (not autopilot) cluster. 
Under networking use the VPC network from above,and under nodes, choose N2 but choose the 4 CPU, 16 GB
machine type - anything else is under powered;
* it will take up to 5 minutes for the cluster to create.  
* While waiting for the cluster to appear, Under VPC Network->IP ADdresses, reserve an External Static IP;
* Under Networks-->Cloud DNS, create a new DNS zone with your domain name. Make sure that you have your 
Name service pointing to the DNS servers that the cloud registry tells you to.
* To your DNS Zone, add "A" records for otcs.<yourdomain>, otds at a minimum (and possibly the others if you
have otac, etc.)

#### GCP artefact creation with Terraform
* cd to infra/terraform 
* review instructions in the README.md there
* Once the prerequisites are set (service account in GCP and keys.json file referenced in terraform.tfvars) run terraform init;
* then run terraform apply

### Helm Repositories required
* There are three repositories required by Helm for this deployment: The opentext one, a ingress-nginx one and for jamstack
* do helm repo ls. There should be 3 entries
* If ingress-nginx is missing add it using
`helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx`
* If jetstack is missing add it using
`helm repo add jetstack https://charts.jetstack.io`
* If OpenText is missing, add it using
`helm repo add opentext https://registry.opentext.com/helm --username <user@example.com> --password <Password>`
where --username and --password are your OT support login creds.
You should put single quotes around both user and password if you
are using /bin/bash, or execute in /bin/sh

### Preparation on the Command line
I used an Ubuntu command line on my local network rather than the GCP CLI.
* make sure that in .env, you have set all the GCP_ params such as PROJECT, ZONE, and CLUSTER;
* run gcloud auth login --no-launch-browser and follow the directions to log the command line into your GCP account;
* run gcloud config set project <GCP_PROJECT>
* run gcloud config set compute/zone <GCP_ZONE>
* run `./setup-secrets.sh` - this script will do a gcloud connect to get kubectl pointing to your context

### Setting up DNS/SSL 
These steps assume you already set up the Cloud DNS in GCP GUI.  It also assumes the previous steps have
successfully created a secret in kubernetes called xecm-secret. Use kubectl get secrets to verify.  Also you should have 3 helm
repositories which can be checked using helm repo ls
* Install the cert manager using the following:

* Do one of:
    * run `./helm-certmgr.sh`
* or
    * `helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --set
installCRDs=true`
    * Deploy cluster issuer: `kubectl create -f ./infra/k8s/cluster-issuer-nginx.yaml`
* It may take a while but eventually you will get the secret updated.  I think though that it requires
the next section - the actual ingress controller first.


### Deployment
* Deploy nginx ingress using either:
    * `helm install otxecm-ingress ingress-nginx/ingress-nginx --set rbac.create=true --set
controller.service.loadBalancerIP=<IP_ADDRESS> --set controller.config.proxy-body-size=1024m``


    * `./helm-ingress <your static ip>` (just added)
* Create database pod using ./helm-otcs-db.sh
* Update helm-cmd-args.properties with any changes. Comment out lines you don't need using #
* Now run `./run-helm-with-args.sh`  - it will deploy the gcp helm chart.  A note with this. for the init container, 
 I needed to add the parameters directly to the above script. When they were in my properties file, they would not load
 properly. Probably something to do with the brackets messing up the copy.  

The above is as far as I've got.
