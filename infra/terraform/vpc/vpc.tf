variable vpc_name {
    type = string
}

# Create a VPC with all the defaults. Name from caller
resource "google_compute_network" "vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = "true"
}

output "vpc_name" {
    value = google_compute_network.vpc.name
}