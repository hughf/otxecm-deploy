variable "domain-name" {
    type = string
}
variable "hostnames" {
    type = list(string)
}

variable "ip-addr" {
    type = string
}

variable "zone-name" {
    type = string
}

resource "google_dns_record_set" "a-rec" {
    count = length(var.hostnames)
    name = "${var.hostnames[count.index]}.${var.domain-name}."
    type = "A"
    ttl = 300
    managed_zone = var.zone-name
    rrdatas = ["${var.ip-addr}"]
}