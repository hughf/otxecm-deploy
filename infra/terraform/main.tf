terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_file)

  project = var.project_id
  region  = var.region
  zone    = var.zone
}

# VPC from module
module "vpcmodule" {
    source = "./vpc"
    vpc_name = "${var.obj_prefix}-${var.project}-vpc"
}

# Static external IP for cluster 

resource "google_compute_address" "ext-ip" {
    name = "${var.obj_prefix}-ext-ip"
    region = var.region 
    address_type = "EXTERNAL"

}

# DNS zone for ingress-nginx
resource "google_dns_managed_zone" "otxecm-zone" {
    name = "${var.obj_prefix}-dns-zone"
    dns_name = "${var.dns-name}."
    description = "DNS zone for OTXECM cluster"
}

# TO DO: Record sets for the DNS zone above
module "recsetmodule" {
    source = "./dnsrec"
    hostnames = ["otcs", "otds", "otac", "otacc", "otpd"]
    domain-name = var.dns-name
    ip-addr = google_compute_address.ext-ip.address
    zone-name = google_dns_managed_zone.otxecm-zone.name

}

module "gkemodule" {
    source = "./k8s"
    gke-name = "${var.obj_prefix}-${var.project_id}-gke"
    gke-network = module.vpcmodule.vpc_name
    gke-zone = var.zone
    project-id = var.project_id
}
