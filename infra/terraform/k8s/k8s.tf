# We want a 3 node cluster for OTXECM
variable "gke_num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "gke-zone" {
    type = string
    default = "us-central1-c"
}

variable "gke-name" {
    type = string
}

variable "gke-network" {
    type = string
}

variable "project-id" {
    type = string
}

# GKE cluster - region from variables.tf and terraform.tfvars
# We are sticking with the stable 1.27 K8S cluster version
data "google_container_engine_versions" "gke_version" {
  location = var.gke-zone
  version_prefix = "1.27."
}

# Name and zone from caller. Note that if we specify 
# a region, it becomes regional, otherwise if location is a zone
# it's a zonal cluster.  Zonal will stick to the default # of nodes
resource "google_container_cluster" "primary" {
  name = var.gke-name
  location = var.gke-zone

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = var.gke-network
  #Subnetting set to auto on VPC
  #subnetwork = google_compute_subnetwork.subnet.name
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = google_container_cluster.primary.name
  location   = var.gke-zone
  cluster    = google_container_cluster.primary.name
  
  version = data.google_container_engine_versions.gke_version.release_channel_latest_version["STABLE"]
  node_count = var.gke_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project-id
    }

    # preemptible  = true
    machine_type = "n2-standard-4"
    tags         = ["gke-node", "${var.project-id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}