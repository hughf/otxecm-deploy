xECM Deployer
=========

Ansible role for deploying xECM.

Requirements
------------

### Supported cloud platforms

| Cloud Platform  | Description |
| --------------- | ----------- |
| gke             | GCP Google Kubernetes Engine (GKE) platform |

### Supported releases

| Release |
| ------- |
| 22.4    |

Role Variables
--------------

| Variable                                                                         | Required | Defaults    | Comments |
| -------------------------------------------------------------------------------- | -------- | ----------- | -------- |
| product_deployer_cloud_platform                                                  | yes      |             | gke |
| product_deployer_environment                                                     | yes      |             | <lab, staging> |
| product_deployer_domain_name                                                     | yes      |  | Example: `otms.opentext.com` |
| product_deployer_helm_repo                                                       | yes      |             | Helm repository where helm chart should be downloaded from. |
| product_deployer_helm_otds_repo                                                       | yes      |             | Helm repository where OTDS helm chart should be downloaded from. |
| product_deployer_image_registry                                                  | yes      |             | Image registry where container images should be downloaded from. |
| product_deployer_customer_name                                                   | yes      |             |  |
| k8s_auth_kubeconfig                                                              | no       | $KUBECONFIG | kubeconfig path to use. |
| xecm_deployer_archive_certificate_file_path                                                       | yes      |             | *to be deprecated* File path to archive certificate file. This will be deprecated once this ansible role include logic for generating certificates. |
| xecm_deployer_otcs_license_file_path                                                       | yes      |             | File path to the otcs license file. |
| xecm_deployer_otpd_license_file_path                                                       | yes      |             | File path to the otpd license file. |
| xecm_deployer_exising_secret_file_path                                                       | yes      |             | File path to the external secrets file. |
| xecm_deployer_ingress_enabled                                                 | yes      | true | Flag to enable or disable xecm ingress |
| xecm_deployer_service_type                                                 | yes      | NodePort | Service type of xecm |
| xecm_deployer_ingress_domain_name                                                | yes      |  | Example: `example.com` |
| xecm_deployer_otcs_public_url                                                    | yes      |            |  |
| xecm_deployer_otds_public_url                                                    | yes      |            |  |
| xecm_deployer_otac_public_url                                                    | yes      |            |  |
| xecm_deployer_otacc_public_url                                                   | yes      |            |  |
| xecm_deployer_otpd_public_url                                                    | yes      |            |  |
| xecm_deployer_image_source                                                       | yes      |  |  |
| xecm_deployer_image_source_public                                                | yes      | docker.io   |  |
| xecm_deployer_pre_upgrade_job_image_source                                                       |   no    | `{{ xecm_deployer_image_source_public }}` |  |
| xecm_deployer_otds_otdsws_migration_pre_upgrade_job_image_source                                 |  no      | `{{ xecm_deployer_image_source_public }}` |  |
| xecm_deployer_otds_otdsws_image_source                                                       |   no     | `{{ xecm_deployer_image_source }}` |  |
| xecm_deployer_otac_image_source                                                       |    no    | `{{ xecm_deployer_image_source }}` |  |
| xecm_deployer_otacc_image_source                                                       |   no     | `{{ xecm_deployer_image_source }}` |  |
| xecm_deployer_otcs_image_source                                                       |    no    | `{{ xecm_deployer_image_source }}` |  |
| xecm_deployer_otpd_image_source                                                       |   no     | `{{ xecm_deployer_image_source }}` |  |
| xecm_deployer_otcs_fluentbit_image_source                                                       |    no    | `{{ xecm_deployer_image_source_public }}` |  |
| xecm_deployer_existing_secret                                                | no      |    | Name of the existing kubernetes external secret |
| xecm_deployer_master_password                                                | yes      |    | Master password for the xecm deployment |
| xecm_master_db_host_name                                                       | yes       |  | Hostname for the external db |
| xecm_master_db_port                                                       | no       | 5432 | Port for the external db |
| xecm_master_db_admin_user_name                                                       | no       | postgres | |
| xecm_master_db_admin_database                                                       | no       | postgres | |
| xecm_deployer_otac_enabled                                                       | no       | true |  |
| xecm_deployer_otds_enabled                                                       | no       | true |  |
| xecm_deployer_otacc_enabled                                                       | no       | false |  |
| xecm_deployer_otac_db_enabled                                                       | no       | false |  |
| xecm_deployer_otcs_enabled                                                       | no       | true |  |
| xecm_deployer_otcs_db_enabled                                                       | no       | false |  |
| xecm_deployer_otiv_enabled                                                       | no       | true |  |
| xecm_deployer_otpd_enabled                                                       | no       | false |  |
| xecm_deployer_otpd_db_enabled                                                       | no       | false |  |
| xecm_deployer_otiv_enabled                                                       | no       | true |  |
| xecm_deployer_global_database_hostname                                                       |   no   | `{{ xecm_master_db_host_name }}` |  |
| xecm_deployer_global_database_admin_username                                                       | no      | `{{ xecm_master_db_admin_user_name }}` | Default : `postgres` |
| xecm_deployer_global_database_admin_database                                                       | no      | `{{ xecm_master_db_admin_database }}` | Default : `postgres` |
| xecm_deployer_global_database_iv_name                                                       |  no     | otiv |  |
| xecm_deployer_global_otds_secret_name                                                       |   no    | otdsws-secrets |  |
| xecm_deployer_global_otds_secret_key                                                       |   no    | OTDS_DIRECTORY_BOOTSTRAP_INITIALPASSWORD |  |
| xecm_deployer_global_db_secret_name                                                       |   no    | otcs-secrets |  |
| xecm_deployer_global_db_secret_key                                                       |   no    | DB_ADMIN_PASSWORD |  |
| xecm_deployer_global_ingress_include_namespace                                                       |   no    | false |  |
| xecm_deployer_global_otds_in_cluster                                                       |   no    | true |  |
| xecm_deployer_global_otds_private_url                                                       |   yes, if xecm_deployer_global_otds_in_cluster = false   |  |  |
| xecm_deployer_global_trusted_source_origins                                                  |  no     | `http://otcs-frontend` |  |
| xecm_deployer_global_trusted_source_origins_anonymous                                          |   no    |  |  |
| xecm_deployer_otds_ingress_enabled                                          |   no    | false |  |
| xecm_deployer_otdsws_otdsdb_dbname                                          |   no     | otdsdb |  |
| xecm_deployer_otdsws_otdsdb_url                                          |   no    | `jdbc:postgresql://{{ xecm_deployer_otdsws_otdsdb_hostname }}:{{ xecm_deployer_otdsws_otdsdb_port }}/{{ xecm_deployer_otdsws_otdsdb_dbname }}` |  |
| xecm_deployer_otdsws_otdsdb_username                                          |  no  | `{{ xecm_master_db_admin_user_name }}` | Default: `postgres` |
| xecm_deployer_otdsws_otdsdb_password                                          |  no     | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otdsws_otdsdb_hostname                                          |   no    | `{{ xecm_master_db_admin_database }}` | Default: `postgres`  |
| xecm_deployer_otdsws_otdsdb_port                                          |    no   | `{{ xecm_master_db_port }}` | Default: `5432` |
| xecm_deployer_otdsws_otadmin_password                                         |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otdsws_migration_enabled                                          |   no    | true |  |
| xecm_deployer_otdsws_migration_using_legacy_image                                          |   no    | true |  |
| xecm_deployer_otdsws_migration_legacy_image_pvc                                          |   no    | otds-data-otds-0 |  |
| xecm_deployer_otdsws_migration_service_name                                          |   no    | opendj |  |
| xecm_deployer_otdsws_migration_service_port                                          |   no    | 1389 |  |
| xecm_deployer_otdsws_migration_opendj_uri:                                          |   no    |  |  |
| xecm_deployer_otdsws_migration_password                                          |   no    |  |  |
| xecm_deployer_otdsws_pre_upgrade_job_enabled                                          |   no    | true |  |
| xecm_deployer_otac_service_annotations                                          |   no    |  |  |
| xecm_deployer_otac_pvc_labels_bdv                                          |    no   |  |  |
| xecm_deployer_otac_pvc_labels_dv                                          |    no   |  |  |
| xecm_deployer_otac_pvc_labels_logs                                          |    no   |  |  |
| xecm_deployer_otac_pvc_labels_opentextlandscape                                          |   no    |  |  |
| xecm_deployer_otac_pvc_labels_otac                                          |    no   |  |  |
| xecm_deployer_otac_pvc_labels_sd                                          |    no   |  |  |
| xecm_deployer_otac_pvc_labels_tomcatlogs                                          |   no    |  |  |
| xecm_deployer_otac_port                                          |   no    | 8080 |  |
| xecm_deployer_otac_protocol                                          |    no   | http |  |
| xecm_deployer_otac_archivename                                          |  no     | A1 |  |
| xecm_deployer_otac_poolname                                          |   no    | Pool1 |  |
| xecm_deployer_otac_volumename                                          |    no   | Vol1 |  |
| xecm_deployer_otac_database_type                                          |    no   | pg |  |
| xecm_deployer_otac_database_name                                          |   no    | ac |  |
| xecm_deployer_otac_database_hostname                                          |   no    | `{{ xecm_master_db_host_name }}` |  |
| xecm_deployer_otac_database_port                                          |   no    | `{{ xecm_master_db_port }}` |  |
| xecm_deployer_otac_database_adminusername                                          |     no  | `{{ xecm_master_db_admin_user_name }}` |  |
| xecm_deployer_otac_database_adminpassword                                          |      no | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otac_database_username                                          |  no     | ac |  |
| xecm_deployer_otac_database_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otac_database_postgres_ts_default                                        | no   | pg_default | |
| xecm_deployer_otac_database_oracle_ts_index                                    | no   | ac_index | |
| xecm_deployer_otac_database_oracle_ts_data                                     | no   | ac_data | |
| xecm_deployer_otac_database_oracle_service_name                                | no   | ac | |
| xecm_deployer_otac_database_oracle_tnsnames_connection_alias                   | no   | ac | |
| xecm_deployer_otac_database_oracle_load_tns_names_enabled                      | no   | false | |
| xecm_deployer_otac_database_oracle_load_tns_names_filename                     | no   | tnsnames.ora | |
| xecm_deployer_otac_otds_admin                                          |   no    | admin |  |
| xecm_deployer_otac_otds_password                                          |   no   | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otacc_port                                          |  no     | 8080 |  |
| xecm_deployer_otacc_cloud_url                                          |   yes,if xecm_deployer_otacc_enabled = true    |  |  |
| xecm_deployer_otacc_cloud_ba_user                                          |  yes,if xecm_deployer_otacc_enabled = true     |  |  |
| xecm_deployer_otacc_cloud_ba_password                                          |   yes,if xecm_deployer_otacc_enabled = true    |  |  |
| xecm_deployer_otacc_connector_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otacc_connector_publichost                                          |   no    | `{{ xecm_deployer_otacc_public_url }}` |  |
| xecm_deployer_otacc_connector_reregister                                          |   no    | false |  |
| xecm_deployer_otacc_connector_insecure                                          |   no    | false |  |
| xecm_deployer_otcs_service_admin_annotations                                          |    no   |  |  |
| xecm_deployer_otcs_service_backendsearch_annotations                                          |  no     |  |  |
| xecm_deployer_otcs_service_frontend_annotations                                          |   no    |  |  |
| xecm_deployer_otcs_pvc_cs_persist_labels                                          |   no    |  |  |
| xecm_deployer_otcs_pvc_logs_labels                                          |    no   |  |  |
| xecm_deployer_otcs_ingress_enabled                                          |   no    | true |  |
| xecm_deployer_otcs_ingress_service_type                                          |   no    | `{{ xecm_deployer_service_type }}` |  |
| xecm_deployer_otcs_containerloglevel                                          |    no   | INFO | Available options: [ INFO, DEBUG ]  |
| xecm_deployer_otcs_shared_address_space_nat_enabled                           |    no   |  false |   |
| xecm_deployer_otcs_passwords_adminuser_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_adminserver_password                                          |  no     | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_sysadmin_password                                          |   no   | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_alfluser_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_bizadmin_password                                          |    no   | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_appmonitor_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_scenario_owner_password                                          |  no     | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_passwords_data_encryptionkey                                          |    no   | /opt/opentext/cs/ |  |
| xecm_deployer_otcs_database_admin_password                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_database_password                                          | no      | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_otacc_corepassword                                          |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otcs_config_url                                          |   no    | `{{ xecm_deployer_otcs_public_url }}` |  |
| xecm_deployer_otcs_config_database_type                                          |   no    | postgres |  |
| xecm_deployer_otcs_config_database_name                                          |   no    | cs |  |
| xecm_deployer_otcs_config_database_hostname                                          |   no    | `{{ xecm_master_db_host_name }}` |  |
| xecm_deployer_otcs_config_database_port                                          |   no    | `{{ xecm_master_db_port }}` | Default: `5432` |
| xecm_deployer_otcs_config_database_useexistingdatabase                                          |   no   | false |  |
| xecm_deployer_otcs_config_database_admin_username                                          |   no    | `{{  xecm_master_db_admin_user_name }}` | Default: `postgres` |
| xecm_deployer_otcs_config_database_admin_pgdatabase                                          |   no    | `{{ xecm_master_db_admin_database }}` | Default: `postgres` |
| xecm_deployer_otcs_config_database_username                                          |   no    | cs |  |
| xecm_deployer_otcs_config_database_autoextend_datafile                                          |    no   | true |  |
| xecm_deployer_otcs_config_database_autoextend_logfile                                          |    no   | true |  |
| xecm_deployer_otcs_config_database_oracle_servicename                                          |   no    | ORCLPDB1 |  |
| xecm_deployer_otcs_config_database_oracle_tnsnames_connection_alias |     no  | ORCL |  |
| xecm_deployer_otcs_config_database_oracle_dbdatafile_spec                                          |  no     | /opt/oracle/cs.dbf |  |
| xecm_deployer_otcs_config_database_oracle_dbdatafile_size                                          |  no     | 500 |  |
| xecm_deployer_otcs_config_database_oracle_load_tnsnames_enabled                           |   no    | false |  |
| xecm_deployer_otcs_config_database_oracle_load_tnsnames_filename                          |  no     | tnsnames.ora |  |
| xecm_deployer_otcs_config_document_storage_type                                           |   no      | otac |  |
| xecm_deployer_otcs_config_document_storage_efs_path                                          |   no    |  |  |
| xecm_deployer_otcs_config_document_storage_efs_storage                                          |   no    |  |  |
| xecm_deployer_otcs_config_document_storage_efs_storage_class_name                  |    no   | nfs |  |
| xecm_deployer_otcs_config_deploy_cws                                          |   no    | true |  |
| xecm_deployer_otcs_config_create_bizadmin_user                                          |   no    | false |  |
| xecm_deployer_otcs_config_create_appmonitor_user                                          |   no    | false |  |
| xecm_deployer_otcs_config_enable_security_logs                                          |   no    | false |  |
| xecm_deployer_otcs_config_extensions_enabled                                          |  no     | false |  |
| xecm_deployer_otcs_config_extensions_include_manifest_init_container                   |   no    | false |  |
| xecm_deployer_otcs_config_extensions_deploy_business_scenarios                        |   no    | false |  |
| xecm_deployer_otcs_config_extensions_deploy_business_scenarios_list                        |  yes, if xecm_deployer_otcs_config_extensions_deploy_business_scenarios = true     | ["OT-EAM","OT-DMS","OT-Teamspaces","OT-Projects","OT-Agreements","OT-HCM","OT-CRM"] |  |
| xecm_deployer_otcs_config_deploy_transport_package                        |     no  | false |  |
| xecm_deployer_otcs_config_transport_packages_url_list                        |  yes, if xecm_deployer_otcs_config_deploy_transport_package = true     |  |  |
| xecm_deployer_otcs_config_valid_http_referers_list                        |   no    |  | Specify list of valid trusted urls |
| xecm_deployer_otcs_config_javaproxy_host                        |   no    |  |  |
| xecm_deployer_otcs_config_javaproxy_port                        |   no    |  |  |
| xecm_deployer_otcs_config_javaproxy_excludes                        |   no    |  |  |
| xecm_deployer_otcs_config_proxy_enabled                        |   no    | false |  |
| xecm_deployer_otcs_config_proxy_host                        |   yes, if xecm_deployer_otcs_config_proxy_enabled = true    |  |  |
| xecm_deployer_otcs_config_proxy_port                        |   yes, if xecm_deployer_otcs_config_proxy_enabled = true    |  |  |
| xecm_deployer_otcs_config_otds_signin_url                        |   no    | `{{ xecm_deployer_otds_public_url }}` |  |
| xecm_deployer_otcs_config_otds_server_url                        |    no   | `{{ xecm_deployer_otds_public_url }}` |  |
| xecm_deployer_otcs_config_otds_samesite_enabled                        |   no    | true |  |
| xecm_deployer_otcs_config_otds_samesite_value                        |   no    | None |  |
| xecm_deployer_otcs_config_otds_trustedSites                        |   no    |  | Specify list of valid trustedsites Urls |
| xecm_deployer_otcs_config_otac_url                        |    no   | http://otac-0:8080 |  |
| xecm_deployer_otcs_config_otac_archive_name                        |    no   | A1 |  |
| xecm_deployer_otcs_config_otac_cert_filename                        |   no    | sp.pem |  |
| xecm_deployer_otcs_config_otacc_url                        |    no   | http://otacc:8080 |  |
| xecm_deployer_otcs_config_otacc_archive_name                        |    no   | A1 |  |
| xecm_deployer_otcs_config_otacc_cert_filename                        |   no    | sp.pem |  |
| xecm_deployer_otcs_config_otacc_core_user                        |   no    | ba.test@username |  |
| xecm_deployer_otcs_config_otacc_archive_description                        |      no | MyArchive |  |
| xecm_deployer_otcs_config_otacc_collection_name                        |    no   | Extended ECM |  |
| xecm_deployer_otcs_config_search_localsearch_enabled                   |    no   |    true          |  |
| xecm_deployer_otcs_config_search_sharedsearch_enabled                   |    no   |     false    |  |
| xecm_deployer_otcs_config_search_sharedsearch_storageclass_name         |    no   |     nfs      |  |
| xecm_deployer_otcs_load_admin_settings_enabled                        |   no    | false |  |
| xecm_deployer_otcs_load_license_enabled                        |   no    | false |  |
| xecm_deployer_otcs_load_license_filename                        |   yes, if xecm_deployer_otcs_load_license_enabled = true    | license.lic | Specify valid license file name |
|xecm_deployer_otcs_content_server_backend_search_shared_search_storage                        |  no     | 1Gi |  |
| xecm_deployer_otcs_fluentbit_enabled                        |      no | false |  |
| xecm_deployer_otcs_logs_to_monitor                        |    yes, if xecm_deployer_otcs_fluentbit_enabled = true   |  | Avalibale options: [ "security" , "sysmon" ] |
| xecm_deployer_otcs_objectimporter_enabled                        |  no     | false |  |
| xecm_deployer_otcs_objectimporter_storage                        |  no     | 1Gi |  |
| xecm_deployer_otcs_init_containers          | no | | Specify list of init containers in `json` format |
| xecm_deployer_otiv_ingress_enabled                        |   no    | false |  |
| xecm_deployer_otiv_otcs_in_cluster                        |   no    | true |  |
| xecm_deployer_otiv_otcs_service_url                        |   no   | http://otcs-frontend |  |
| xecm_deployer_otiv_amqp_rabbitmq_password                        |   yes, if xecm_deployer_otiv_enabled = true    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_service_annotations                        |    no   |  |  |
| xecm_deployer_otpd_pvc_labels_pd_persist_logs                        |   no    |  |  |
| xecm_deployer_otpd_service_type                        |      no | `{{ xecm_deployer_service_type }}` |  |
| xecm_deployer_otpd_service_name                        |     no  | otpd |  |
| xecm_deployer_otpd_load_license                        |      no | true |  |
| xecm_deployer_otpd_license_file                        |    yes, if xecm_deployer_otpd_load_license = true   | otpdlicense.lic | Specify valid otpd license name |
| xecm_deployer_otpd_public_hostname                        |  no     | `{{ xecm_deployer_otpd_public_url }}` |  |
| xecm_deployer_otpd_port                        |    no   | 8080 |  |
| xecm_deployer_otpd_target_port                        |   no    | 8080 |  |
| xecm_deployer_otpd_protocol                        |   no    | http |  |
| xecm_deployer_otpd_admin                        |  no     |powerdocsadmin |  |
| xecm_deployer_otpd_admin_password                        |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_user                        |  no    | powerdocsuser |  |
| xecm_deployer_otpd_user_password                        |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_monitor_user                        |   no    | powerdocsmonitoruser |  |
| xecm_deployer_otpd_monitor_user_password                        |     no  | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_otds_admin                        |    no   | admin |  |
| xecm_deployer_otpd_otds_password                        |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_otcs_enabled                        |   no    | true |  |
| xecm_deployer_otpd_database_type                        |   no    | pg |  |
| xecm_deployer_otpd_database_hostname                        |   no    | `{{ xecm_master_db_host_name }}` |  |
| xecm_deployer_otpd_database_port                        |    no   | `{{ xecm_master_db_port }}` | Default: `5432` |
| xecm_deployer_otpd_database_admin_database                        |   no    | `{{ xecm_master_db_admin_database }}` | Default: `postgres` |
| xecm_deployer_otpd_database_admin_username                        | no      | `{{ xecm_master_db_admin_user_name }}` | Default: `postgres` |
| xecm_deployer_otpd_database_admin_password                        |   no    | `{{ xecm_deployer_master_password }}` |  |
| xecm_deployer_otpd_database_name                        |   no    | pddocgen |  |
| xecm_deployer_otpd_database_username                        |    no   | pddocgen |  |
| xecm_deployer_otpd_database_password                        |   no    | `{{ xecm_deployer_master_password }}` |  |


**NOTE** 
- By default we are not relying on container databases instead all the databases of (OTIV, OTAC, OTCS, OTDS, OTPD) are pointed towards a master database.
- Individual database configuration can be done according to desired service (OTIV, OTAC, OTCS, OTDS, OTPD). Required variables for individual configurations are listed as in above table.