{{/*
Validation of values provided by an existing Kubernetes secret
*/}}
{{- $secret_object := lookup "v1" "Secret" .Release.Namespace .Values.global.existingSecret }}
{{- if and (ne .Values.global.existingSecret "otxecm-default-secrets") $secret_object }}
	{{- if not $secret_object.data }}
		{{- fail "\n\nError: keys from the existing secret set at .Values.global.existingSecret must be defined under the data section.\n" }}
	{{- end }}
	{{- $secrets := $secret_object.data }}
	{{- if eq .Values.global.otcs.enabled true }}
		{{- if not $secrets.DATA_ENCRYPTION_KEY }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a DATA_ENCRYPTION_KEY.\n" }}
		{{- else if not $secrets.ADMIN_USER_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set an ADMIN_USER_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if not $secrets.ADMIN_SERVER_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set an ADMIN_SERVER_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if not $secrets.AUTO_SYS_ADMIN_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set an AUTO_SYS_ADMIN_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if not $secrets.ALFILTER_USER_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set an ALFILTER_USER_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if not $secrets.DB_ADMIN_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a DB_ADMIN_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if not $secrets.DB_PASSWORD }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a DB_PASSWORD since global.otcs.enabled is true.\n" }}
		{{- else if and (eq .Values.otcs.config.documentStorage.type "otacc") (not $secrets.AC_CORE_PASSWORD) }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a AC_CORE_PASSWORD since OTCS is set to use OTACC.\n" }}
		{{- else if and (eq .Values.otcs.config.createBizadminUser true) (not $secrets.BIZ_ADMIN_PASSWORD) }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a BIZ_ADMIN_PASSWORD since otcs.config.createBizadminUser is true.\n" }}
		{{- else if and (eq .Values.otcs.config.createAppMonitorUser true) (not $secrets.APPMONITOR_PASSWORD) }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a APPMONITOR_PASSWORD since otcs.config.createAppMonitorUser is true.\n" }}
		{{- else if and (eq .Values.otcs.config.deployBusinessScenarios true) (not $secrets.SCENARIO_OWNER_PASSWORD) }}
			{{- fail "\n\nError: existing secret from global.existingSecret must set a SCENARIO_OWNER_PASSWORD since otcs.config.deployBusinessScenarios is true.\n" }}
		{{- end }}
	{{- end }}
	{{- if eq .Values.global.otac.enabled true }}
		{{- if not $secrets.TARGET_DB_PASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a TARGET_DB_PASSWORD since .Values.global.otac.enabled is true.\n" }}
		{{- else if not $secrets.OTAC_DB_ADMIN_PASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a OTAC_DB_ADMIN_PASSWORD since .Values.global.otac.enabled is true.\n" }}
		{{- else if not $secrets.OTDS_PASS }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set an OTDS_PASS since .Values.global.otac.enabled is true.\n" }}
		{{- end }}
	{{- end }}
	{{- if eq .Values.global.otacc.enabled true }}
		{{- if not $secrets.BA_PASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a BA_PASSWORD since .Values.global.otacc.enabled is true.\n" }}
		{{- else if not $secrets.CONNECTOR_PASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a CONNECTOR_PASSWORD since .Values.global.otacc.enabled is true.\n" }}
		{{- end }}
	{{- end }}
	{{- if eq .Values.global.otpd.enabled true }}
		{{- if eq .Values.otpd.otcs.enabled true }}
			{{- if not $secrets.OTCS_ADMIN_PASSWORD }}
				{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a OTCS_ADMIN_PASSWORD since .Values.otpd.otcs.enabled is true.\n" }}
			{{- else if ne $secrets.ADMIN_USER_PASSWORD $secrets.OTCS_ADMIN_PASSWORD }}
				{{- fail "\n\nError: existing secret from .Values.global.existingSecret must have the same values for ADMIN_USER_PASSWORD and OTCS_ADMIN_PASSWORD.\n" }}
			{{- end }}
		{{- end}}
		{{- if eq .Values.otpd.emailServerSettings.enabled true }}
			{{- if and (.Values.otpd.emailServerSettings.user) (not $secrets.EMAIL_SERVER_PASSWORD) }}
				{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set an EMAIL_SERVER_PASSWORD since .Values.otpd.emailServerSettings.enabled is set to true and .Values.otpd.emailServerSettings.user has a value set.\n" }}
			{{- end }}
			{{- if and (empty .Values.otpd.emailServerSettings.user) ($secrets.EMAIL_SERVER_PASSWORD) }}
				{{- fail "\n\nError: existing secret from .Values.global.existingSecret must not set an EMAIL_SERVER_PASSWORD since .Values.otpd.emailServerSettings.enabled is set to true but .Values.otpd.emailServerSettings.user has no value set.\n" }}
			{{- end }}
		{{- end}}
	{{- end }}
	{{- if eq .Values.global.otiv.enabled true }}
		{{- if eq .Values.otiv.amqp.enabled true}}
			{{- if not (index $secrets "rabbitmq-password") }}
				{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set rabbitmq-password since .Values.global.otiv.enabled is true" }}
			{{- end }}
		{{- end }}
		{{- if not $secrets.ADMIN_USER_PASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must set a ADMIN_USER_PASSWORD.\n" }}
		{{- end }}
	{{- end }}
	{{/*
	Ensure password synchronization between OTCS, OTDS and OTAC. 
	*/}}
	{{- if and (eq .Values.global.otcs.enabled true) (eq .Values.global.otds.enabled true) }}
		{{- if ne $secrets.ADMIN_USER_PASSWORD $secrets.OTDS_DIRECTORY_BOOTSTRAP_INITIALPASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must have the same values for ADMIN_USER_PASSWORD and OTDS_DIRECTORY_BOOTSTRAP_INITIALPASSWORD.\n" }}
		{{- end }}
	{{- end }}
	{{- if and (eq .Values.global.otac.enabled true) (eq .Values.global.otds.enabled true) }}
		{{- if ne $secrets.OTDS_PASS $secrets.OTDS_DIRECTORY_BOOTSTRAP_INITIALPASSWORD }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must have the same values for OTDS_PASS and OTDS_DIRECTORY_BOOTSTRAP_INITIALPASSWORD.\n" }}
		{{- end }}
	{{- end }}
	{{- if and (eq .Values.global.otcs.enabled true) (eq .Values.global.otac.enabled true) }}
		{{- if ne $secrets.ADMIN_USER_PASSWORD $secrets.OTDS_PASS }}
			{{- fail "\n\nError: existing secret from .Values.global.existingSecret must have the same values for ADMIN_USER_PASSWORD and OTDS_PASS.\n" }}
		{{- end }}
	{{- end }}
{{- end }}

{{/*
Ensure oracle database has the correct values.
*/}}
{{- if and (ne .Values.otcs.config.database.type "oracle") (ne .Values.otcs.config.database.type "postgres")}}
	{{- fail "otcs.config.database.type must be either postgres or oracle" }}
{{- end}}
{{- if and (eq .Values.otcs.config.database.type "oracle") (eq .Values.global.otcs.enabled true)}}
	{{- if and (eq .Values.otcs.config.database.oracle.loadTnsnames.enabled true) (not .Values.otcs.config.database.oracle.tnsnamesConnectionAlias )}}
		{{- fail "otcs.config.database.oracle.tnsnamesConnectionAlias must be set to the connection alias to be used from the tnsnames file if otcs.config.database.oracle.loadTnsnames.enabled true" }}
	{{- end}}
	{{- if and (eq .Values.otcs.config.database.oracle.loadTnsnames.enabled false) (not .Values.otcs.config.database.oracle.serviceName )}}
		{{- fail "otcs.config.database.oracle.serviceName must be set to the pluggable database to use if otcs.config.database.oracle.loadTnsnames.enabled false" }}
	{{- end}}
	{{if (eq .Values.otcs.config.database.adminUsername "sys")}}
		{{- fail ".Values.otcs.config.database.adminUsername do not use sys user, instead use system."}}
	{{- end}}
{{- end}}

{{- if and (eq .Values.global.otcs.enabled true) (eq .Values.global.otds.enabled true) }}
	{{- if ne (int (.Values.otcs.config.otds.port)) (int (.Values.otds.otdsws.port)) }}
		{{- fail "otcs.config.otds.port and otds.otdsws.port do not match. The port must be the same." }}
	{{- end }}
{{- end }}

{{- if and (eq .Values.global.otac.enabled true) (eq .Values.global.otds.enabled true) }}
	{{- if ne (int (.Values.otac.otds.port)) (int (.Values.otds.otdsws.port)) }}
		{{- fail ".Values.otac.otds.port and .Values.otds.otdsws.port do not match. The port must be the same." }}
	{{- end }}
{{- end }}

{{- if eq .Values.otcs.config.syndication.enabled true }}
    {{/*
    Validation of values when syndication is enabled. It checks for all the syndication values are empty
    */}}
	{{- if eq ( default .Values.global.serviceType .Values.otcs.serviceType | toString) "LoadBalancer"}}
		{{- if or (empty .Values.otcs.config.syndication.sitename) (empty .Values.otcs.config.syndication.port) }}
			{{- fail ".Values.otcs.config.syndication.sitename or .Values.otcs.config.syndication.port must be set since syndication is enabled." }}
	    {{- end }}
	{{- else }}
		{{- if or (empty .Values.otcs.config.syndication.sitename) (empty .Values.otcs.config.syndication.port) (empty .Values.otcs.config.syndication.qdsUrl) }}
			{{- fail ".Values.otcs.config.syndication.sitename or .Values.otcs.config.syndication.port or .Values.otcs.config.syndication.qdsUrl must be set since syndication is enabled." }}
		{{- end }}
	{{- end }}
	{{- if and (empty .Values.otcs.config.syndication.siteid) (ne .Values.otcs.config.syndication.siteid 0) }}
		{{- fail ".Values.otcs.config.syndication.siteid must be set since syndication is enabled." }}
	{{- end }}
	{{/*
    If primary is true,checks for siteid is zero
    */}}
	{{- if eq .Values.otcs.config.syndication.isPrimary true }}
		{{- if ne .Values.otcs.config.syndication.siteid 0 }}
			{{- fail ".Values.otcs.config.syndication.siteid must be zero." }}
		{{- end }}
	{{/*
    If primary is false,checks for siteid is not zero
    */}}
	{{ else }}
		{{- if eq .Values.otcs.config.syndication.siteid 0 }}
			{{- fail ".Values.otcs.config.syndication.siteid must not be zero." }}
		{{- end }}
	{{- end }}
{{- end }}

{{/*
If prometheus metrics is enabled. It checks for availabilty of key 
*/}}
{{- if eq .Values.global.otcs.enabled true }}
{{- if and (empty .Values.otcs.config.prometheusMetrics.key) (eq .Values.otcs.config.prometheusMetrics.enabled true) }}
	{{- fail ".Values.otcs.config.prometheusMetrics.key must be set since prometheus metrics of adminserver processes are enabled." }}
{{- end }}
{{- end }}

{{- if (eq .Values.otcs.config.contentProtection.enabled true) }}
	{{- if or (empty .Values.otcs.config.contentProtection.storage) (empty .Values.otcs.config.contentProtection.path) }}
		{{- fail ".Values.otcs.config.contentProtection.storage or .Values.otcs.config.contentProtection.path should be set"  }}
	{{- end }}
{{- else if and (ne .Values.global.existingSecret "otxecm-default-secrets") (not $secret_object) }}
	{{- fail "\n\nError: existing secret defined at .Values.global.existingSecret not found.\n" }}
{{- end }}


# Validation for otds connection url with proper DB name
{{- if and (not (eq (regexFind "([^\\/]+)$" .Values.otds.otdsws.otdsdb.url) .Values.otds.otdsws.otdsdb.automaticDatabaseCreation.dbName)) (eq .Values.otds.otdsws.otdsdb.automaticDatabaseCreation.enabled true) }}
{{- fail "Update the otds connection string url with appropriate otds database name which should be same as .Values.otds.otdsws.otdsdb.automaticDatabaseCreation.dbName." }}
{{- end }}


# Ensure PowerDocs charts has the correct values.
{{- if eq .Values.global.otpd.enabled true }}
	{{- if ne .Values.global.otcs.enabled .Values.otpd.otcs.enabled }}
		{{- fail ".Values.global.otcs.enabled and .Values.otpd.otcs.enabled must be set to same value." }}
	{{- end }}
	{{- if eq .Values.otpd.emailServerSettings.enabled true }}
		{{/*
		Validation of values when Email Server Setting is enabled.
		*/}}
		{{- if or (empty .Values.otpd.emailServerSettings.server) (empty .Values.otpd.emailServerSettings.port) }}
			{{- fail ".Values.otpd.emailServerSettings.server and .Values.otpd.emailServerSettings.port must be set since .Values.otpd.emailServerSettings.enabled is set to true." }}
		{{- end }}
		{{- if eq .Values.global.existingSecret "otxecm-default-secrets" }}
			{{- if and (empty .Values.otpd.emailServerSettings.user) (.Values.otpd.emailServerSettings.password) }}
				{{- fail "Cannot leave .Values.otpd.emailServerSettings.user empty since .Values.otpd.emailServerSettings.password is set." }}
			{{- end }}
			{{- if and (.Values.otpd.emailServerSettings.user) (empty .Values.otpd.emailServerSettings.password) }}
				{{- fail "Cannot leave .Values.otpd.emailServerSettings.password empty since .Values.otpd.emailServerSettings.user is set." }}
			{{- end }}
		{{- end }}
	{{- end }}
{{- end }}

# Validation for license secret
{{- if .Values.global.existingLicenseSecret }}
	{{- $license_secret := lookup "v1" "Secret" .Release.Namespace .Values.global.existingLicenseSecret }}
	{{- if and $license_secret $license_secret.data }}
		{{- if and (eq .Values.otcs.loadLicense.enabled true) (not (index $license_secret.data .Values.otcs.loadLicense.filename )) }}
			{{- fail ".Values.otcs.loadLicense.filename not found in license secret"}}
		{{- end }}
	{{- else }}	
		{{- fail ".Values.global.existingLicenseSecret does not exist or doesn't have data"}}
	{{- end }}
{{- end }}

# Validation for otac certificate secret
{{- if .Values.otcs.config.otac.certSecret }}
	{{- $otac_secret := lookup "v1" "Secret" .Release.Namespace .Values.otcs.config.otac.certSecret }}
	{{- if and $otac_secret $otac_secret.data }}
		{{- if not (index $otac_secret.data .Values.otcs.config.otac.certFilename )}}
			{{- fail ".Values.otcs.config.otac.certFilename not found in certificate secret"}}
		{{- end }}
	{{- else }}	
		{{- fail ".Values.otcs.config.otac.certSecret does not exist or does not have data"}}
	{{- end }}
{{- end }}

# Validation for otacc certificate secret
{{- if .Values.otcs.config.otacc.certSecret }}
	{{- $otacc_secret := lookup "v1" "Secret" .Release.Namespace .Values.otcs.config.otacc.certSecret }}
	{{- if and $otacc_secret $otacc_secret.data }}
		{{- if not (index $otacc_secret.data .Values.otcs.config.otacc.certFilename )}}
			{{- fail ".Values.otcs.config.otacc.certFilename not found in certificate secret"}}
		{{- end }}
	{{- else }}
		{{- fail ".Values.otcs.config.otacc.certSecret does not exist or does not have data"}}
	{{- end }}
{{- end }}

# Validation for pre-existing adminsettings configmaps
{{- if and (.Values.otcs.loadAdminSettings.enabled) (.Values.otcs.loadAdminSettings.initialConfigmap)}}
	{{- $initial_configmap := lookup "v1" "ConfigMap" .Release.Namespace .Values.otcs.loadAdminSettings.initialConfigmap }}
	{{- if not ($initial_configmap) }}
		{{- fail ".Values.otcs.loadAdminSettings.initialConfigmap does not exist"}}
	{{- end }}
{{- end }}

{{- if and (.Values.otcs.loadAdminSettings.enabled) (.Values.otcs.loadAdminSettings.recurrentConfigmap)}}
	{{- $recurrent_configmap := lookup "v1" "ConfigMap" .Release.Namespace .Values.otcs.loadAdminSettings.recurrentConfigmap }}
	{{- if not ($recurrent_configmap) }}
		{{- fail ".Values.otcs.loadAdminSettings.recurrentConfigmap does not exist"}}
	{{- end }}
{{- end }}