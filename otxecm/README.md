# OpenText Extended ECM

[Extended ECM](https://www.opentext.com/products-and-solutions/products/enterprise-content-management/extended-ecm-platform) is the market leading Enterprise Content Management platform that integrates content and content management in leading business applications such as SAP, Salesforce and Microsoft.

This Helm chart supports the deployment of Extended ECM on Kubernetes platforms. It packages the Extended ECM chart (Content Server) and all necessary components (Archive Center, Core Archive Connector, Directory Services, Intelligent Viewing and PowerDocs)

Please, refer to the product release notes in OpenText My Support to make sure to only use supported environments for productive use.

## TL;DR

Untar the helm chart. Edit the `otxecm/platforms/<platform>.yaml` for the platform you are deploying on and update the `imageSource` field for your docker registry.

If you do not have a domain/DNS created and are using kubernetes load balancers for testing purposes, edit `otxecm/platforms/<platform>.yaml` and set ingress_enabled to false, and update the global `xxxxPublicUrl` fields to empty strings "".

If you have a domain/DNS, edit the `otxecm/platforms/<platform>.yaml` for your platform and update the various `xxxxPublicUrl` fields. You will need to create a kubernetes secret for your TLS certificate following the detailed instructions below in this document. You will need to enable a nginx controller and include a static IP for it.

Next, deploy the helm chart:

```console
helm install otxecm otxecm -f otxecm/platforms/<platform>.yaml \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
```

**NOTE:** In 23.4 the otcs-db, otac-db, and otpd-db subcharts were removed. The corresponding services must connect to an existing database.

## Introduction

This chart bootstraps an [Extended ECM](https://www.opentext.com/products-and-solutions/products/enterprise-content-management/extended-ecm-platform) deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Install [Docker](https://docs.docker.com/get-started) (to push and pull Container images)
- Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) CLI in your local environment
- Install [Helm](https://helm.sh/docs/using_helm/#installing-helm) toolset on local environment (use 3.02 or newer)
- Create Kubernetes cluster in your cloud infrastructure. Minimum of 3 nodes with 4 CPU and 15GB storage each is required for a non-production setup.
- A sp.pem certificate file for installations with Archive Server or Archive Core Connector must be created in the folder otxecm/charts/otcs/.
- **We have introduced the ability to set the timezone for the entire deployment. It is strongly recommended that you keep the entire deployment in the same time zone (regardless of whether it is completely containerized, split into multiple namespaces, a mix of containerized / managed or on-premise) as this could cause undesired side effects with processes going off at unexpected times, incorrect date/time stamping etc.**
  **In order to set this, the _timeZone_ value in the platform files needs to be modified. The default for the deployment is Etc/UTC. If you would like to change this, please ensure that you accurately set this value to a known supported value [List of tz database time zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).**

## Validating the Chart

> **Tip**: List all releases using `helm list`

To test and check the chart:

```console
helm template -f otxecm/platforms/<platform>.yaml  otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='

helm install \
otxecm otxecm \
-f otxecm/platforms/<platform>.yaml \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--dry-run \
--debug
```

## Installing the Chart

You need to be in the folder that includes the `otxecm` chart folder.
To install the chart with a dynamically assigned release name:

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
```

Replace `<platform>` with one of the supported platform files in that folder.

Edit the `otxecm/platforms/<platform>.yaml` to your needs (e.g., enable / disable Ingress, set domain names and host names)

The command deploys OpenText Extended ECM on the Kubernetes cluster together with necessary components (Directory Services, Archive Center, Core Archive Connector, Intelligent Viewing and PowerDocs). The [configuration](#configuration) section lists the parameters that can be configured during installation.

## Deploying with an existing kubernetes secret

The use of kubernetes secret file is intended for production deployments. Edit otxecm/example-secret.yaml and provide password values for any components you are using. For example, if you are not using otac (Archive Center) for storage, then you can ignore those passwords. Provided values must be base64 encoded, per kubernetes requirements. If you are encoding in Linux with the base64 command, make sure you do not include new line characters. For example:

```console
echo -n 'aBigLongStringToEncode' | base64 -w 0
```

Create the kubernetes secret with the following command:

```console
kubectl create -f otxecm/example-secret.yaml
```

Also provide the secret name in the `otxecm/<platform>.yaml` file by replacing otxecm-default-secrets. By default, the secret name in example-secret.yaml is otxecm-secrets, so the line in `otxecm/<platform>.yaml` would look like this:

```console
existingSecret:                                    otxecm-secrets
```

The command to deploy the helm chart is the same as above.

> **Important:** When deploying using secrets, all keys specified in the example-secret.yaml must be set for the containers that you are using. The exception to this is optional components that you are not using. For example, if you are using an otxecm container in your deployment, but not otac (Archive Server) as storage, then you do not need the AC_CORE_PASSWORD key under the ##otcs section.

## Optionally deploying with the Master password

Intended for non-production environments, where you can set a unique password across all components. To set the Master password, edit the otxecm/platforms/<platform>.yaml file and define the &master_password. In this case a new secret with name otxecm-default-secrets gets created and it will be used as existing_secret.

You can overwrite the password for a specific component based on the tables below:

#### OTDS

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set otds.otdsws.otadminPassword=\<password>                               | Define the password for the otadmin@otds.admin user for the OTDS        |
| --set otds.otdsws.otdsdb.password=\<password>                               | Define the password for the otds-db                                     |

#### OTCS

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set otcs.passwords.adminUserPassword=\<password>                              | Define the Content Server Administration User Password                       |
| --set otcs.passwords.adminServerPassword=\<password>                              | Define the Content Server Administration Server Password                       |
| --set otcs.passwords.baPassword=\<password>                                 | Define the password for the bizadmin user for the Content Server        |
| --set otcs.passwords.appMonitorPassword=\<password>                         | Define the password for the appmonitor user for the Content Server      |
| --set otcs.passwords.scenarioOwnerPassword=\<password>                      | Define the password for the Scenario Owner user for the Content Server  |
| --set otcs.passwords.database.adminPassword=\<password>                     | Define the password for the admin user for the Content Server database  |
| --set otcs.passwords.database.password=\<password>                          | Define the password for the user that owns the Content Server database  |

#### OTACC

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set otcs.passwords.otacc.corePassword=\<password>                         | Define the password for the  otacc user, if otacc is being used for storage|
| --set otacc.cloud.baPassword=\<password>                                    | Define the password for the Business Administrator in Core Archive         |
| --set otacc.connector.Password=\<password>                                  | Define the password for the internal  Core Archive Connector user          |

#### OTAC

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set otac.database.adminPassword=\<password>                               | Define the password for the admin user for the Archive Center database     |
| --set otac.database.password=\<password>                                    | Define the password for the user that owns the Archive Center database     |
| --set otac.otds.password=\<password>                                        | Define the password for the administrator user for the OTDS                |

#### OTIV

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set global.database.adminPassword=\<password>                             | Define the password for the admin user for the config, publication, publisher, and markup services                     |
| --set global.masterPassword=\<password>                                     | Define the master password for all the otiv services                       |
| --set otiv.amqp.rabbitmq.password=\<password>                               | Define the password for the messaging user for otiv-amqp                   |

#### OTPD

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|
| --set otpd.adminPassword=\<password>                                        | Define the password for the admin user for the PowerDocs                   |
| --set otpd.userPassword=\<password>                                         | Define the password for the PowerDocs user                 |
| --set otpd.monitorUserPassword=\<password>                                  | Define the password for the monitorUser User for the PowerDocs             |
| --set otpd.apiUserPassword=\<password>                                      | Define the password for the apiUser User for the PowerDocs                 |
| --set otpd.database.adminPassword=\<password>                               | Define the password for the admin user for the PowerDocs database          |
| --set otpd.database.password=\<password>                                    | Define the password for the user that owns the PowerDocs database          |
| --set otpd.otcs.password=\<password>                                        | Define the password for the administrator user for the OTCS                |
| --set otpd.otds.password=\<password>                                        | Define the password for the administrator user for the OTDS                |

#### Databases

| Command Line Parameter                                                 | Description                                                         |
|------------------------------------------------------------------------|---------------------------------------------------------------------|

## Sample Command Line Parameters

These are common command line parameters that can be used. Please view the specific chart `values.yaml` file for all available options. For example, look in otxecm/charts/otcs/values.yaml for all values to be used with the otcs chart.

| Command Line Parameter                                                 | Description                                                         | Example Values                            |
|------------------------------------------------------------------------|---------------------------------------------------------------------|-------------------------------------------|
| --set otcs.image.name=\<name>                                           | Define the name of the Extended ECM Docker image   | otxecm, otxecm-documentum-sap        |
| --set otcs.image.tag=\<version>                                         | Define the Docker image tag / version of the Extended ECM image   | 22.2.0                                    |
| --set otac.image.tag=\<version>                                         | Define the Docker image / tag version of the Archive Center image   | 22.4.0                                    |
| --set otds.otdsws.image.tag=\<version>                                         | Define the Docker image / tag version of the Directory Services image             | 22.3.0                                    |
| --set otds.otdsws.otdsdb.url=\<url>                                         | Define the url to connect to the DB for the Directory Services             | jdbc:postgresql://\<db-hostname>:5432/otdsdb                                    |
| --set otds.otdsws.otdsdb.username=\<name>                                         | Define the username for the otds-db             | postgres                                    |
| --set global.otac.enabled=\<boolean>                                           | Define if Archive Center gets deployed as container or not  (enabled by default)        | false, true                               |
| --set global.otiv.enabled=\<boolean>                                           | Define if Intelligent Viewing services are deployed (enabled by default)         | false, true                               |
| --set otcs.config.documentStorage.type=\<store>                                        | Define where the content gets stored (needed if otac.enabled=false) | database, otac, otacc, efs      |
| --set otcs.config.database.hostname=<host/IP>                                          | Define the hostname of the DB server (if it is outside the cluster) | IP address or fully qualified domain name |
| --set otcs.config.database.adminUsername=\<database admin>                                     | Define the admin username of the DB server  | postgres |
| --set otcs.config.port=\<port number>                                    | Defines the external port for the otcs kubernetes service. Cannot be changed after initial deployment. | 45312 |
| --set otcs.config.enableMultiProcessMode=\<boolean>                                    | Defines if MultiProcessMode should be enabled or disabled. | false, true |
| --set otds.port=\<port number>                                    | Defines the external port for the otds kubernetes service. Cannot be changed after initial deployment. | 16254 |
| --set otds.otcsPort=\<port number>                                    | Defines the external port for the otcs kubernetes service. Cannot be changed after initial deployment. This must match the otcs port defined for the otds helm chart. | 45312 |
| --set global.otpd.enabled=\<boolean>                                           | Define if PowerDocs container is deployed          | false, true  |
| --set otcs.contentServerFrontend.replicas=\<num>                        | Number of Content Server Frontend instances to start. If set to 0 the admin pod will receive the traffic from the otcs-frontend service, when running a helm install or upgrade command              | 0-n                                       |
| --set otcs.contentServerFrontend.resources.requests.cpu=\<num>          | Number of CPU to be requested for Content Server Frontend           | 1                                         |
| --set otcs.contentServerFrontend.resources.requests.memory=\<gigabytes> | Compute memory to be requested for Content Server frontend          | 1.5Gi                                     |
| --set otcs.contentServerFrontend.limits.requests.cpu=\<num>             | CPU limit for Content Server Frontend                               | 2                                         |
| --set otcs.contentServerFrontend.limits.requests.memory=\<gigabytes>    | Compute memory limit for Content Server frontend                    | 4Gi                                       |
| --set otcs.contentServerAdmin.resources.requests.cpu=\<num>             | Number of CPU to be requested for Content Server Frontend           | 1                                         |
| --set otcs.contentServerAdmin.resources.requests.memory=\<gigabytes>    | Compute memory to be requested for Content Server frontend          | 1.5Gi                                     |
| --set otcs.contentServerAdmin.limits.requests.cpu=\<num>                | CPU limit for Content Server Frontend                               | 2                                         |
| --set otcs.contentServerAdmin.limits.requests.memory=\<gigabytes>       | Compute memory limit for Content Server frontend                    | 4Gi                                       |
| --set otcs.containerLogLevel=\<string>                       | Define how much information is logged by the container setup. 'DEBUG' will also enable Content Server logs during and after the deployment. | DEBUG, INFO, WARNING, ERROR, CRITICAL                                       |
| --set otcs.sharedAddressSpaceNat.enabled=\<boolean>                             | Define whether to allow Tomcat internalProxies to accept 100.64.0.0/10 IP range(Carrier-grade NAT) | false, true                                       |
|
| --set otpd.emailServerSettings.enabled=\<boolean>                       | Define if email server settings should be updated during deployment| false, true                               |
| --set otpd.emailServerSettings.server=\<string>                         | A valid email server                                               | smtp.office365.com                        |
| --set otpd.emailServerSettings.port=\<port number>                      | A valid email server port                                          | 587                                       |
| --set otpd.emailServerSettings.user=\<string>                           | A valid email server user                                          | username                                  |
| --set otpd.emailServerSettings.password=\<password>                     | A valid email server password                                      | password                                  |

### Install with defined release name

To install the chart with the release name `my-release`:

```console
helm install my-release otxecm -f otxecm/platforms/<platform>.yaml \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
```

### Install with different Extended ECM docker image

To change the Extended ECM Docker image (e.g., Extended ECM = `otxecm`, Extended ECM Documentum for SAP = `otxecm-documentum-sap`):

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--set otcs.image.name=otxecm
```

### Install without Archive Center

To install the chart without the OpenText Archive Center container but use database content store use these parameters:

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--set global.otac.enabled=false \
--set otcs.config.documentStorage.type=database
```

To use external filesystem as content store (in fact these are volumes on the Kubernetes platform) you can do so by setting the `otcs.config.documentStorage.type` variable to `efs`. You may also need to adjust the `otcs.config.documentStorage.efsStorageClassName` variable (`gcp-nfs` is just an example from Google Cloud Platform).

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--set global.otac.enabled=false \
--set otcs.config.documentStorage.type=efs \
--set otcs.config.documentStorage.efsStorageClassName=gcp-nfs
```

### Install with existing Archive Center outside the Cluster

You may already have a central instance of Archive Center to use, or when using OTK you can use Core Archive (Archive Server cloud storage). To install the chart without the OpenText Archive Center container:

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--set global.otac.enabled=false \
--set otcs.config.otac.url=<URL>
```

To install the chart without the OpenText Archive Center container and use otacc (OpenText hosted platform only):

1. Install the Core Archive connector helm chart. Make sure to edit the values.yaml with your archive connection details.

```console
helm install \
otxecm otxecm \
-f otxecm/platforms/cfcr.yaml \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA==' \
--set otcs.config.documentStorage.type=otacc \
--set otcs.config.otacc.archiveName=<desired unique archive name> \
--set otcs.config.otacc.collectionName=<desired unique collection name> \
--set otcs.passwords.otacc.corePassword=<core archive cloud password> \
--set otcs.config.otacc.coreUser=<core archive cloud username> \
--set global.otac.enabled=false \
--set global.otacc.enabled=true  \
--set otacc.cloud.baUser=<core archive cloud username> \
--set otacc.cloud.baPassword=<core archive cloud password> \
--set otacc.connector.password=<core archive connector password> \
--set otacc.cloud.url=https://saribung.otxlab.net \
--set otacc.connector.reregister=true
```

Replace fields marked in angle brackets `<>` with values for your deployment.

### Install with existing OpenText Directory Services outside the Cluster

To install the chart without the OpenText Directory Services container (because you have a central instance of Directory Services already deployed):

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml otxecm \
--set global.otds.enabled=false
```

Replace global.otdsPublicUrl inside `otxecm/platforms/<platform>.yaml` with the url of your existing OTDS.
The OpenText Directory Services server url can be set with the helm parameter:
`--set otcs.config.otds.serverUrl="yourOTDSUrl.com"`

The OpenText Directory Services sign in url can be set with the helm parameter:
`--set otcs.config.otds.signInUrl="yourOTDSSignIn.com"`

Both serverUrl and signInUrl are defaulted to the global.otdsPublicUrl when using an external OTDS.

### Install with DNS names and HTTPS using a Kubernetes Ingress

Edit the `global` section in the `platforms/<platform>.yaml` in the Helm chart directory. You need to have DNS entries and certificates for SSL / HTTPS prepared. Some platforms may also need a static IP to be created upfront or an ingress controller such as nginx-ingress.

### Install with predefined deployment sizes

Inside the `sizings/` folder in the Helm chart directory you find several deployment size examples. You can use them to customize the resources in your deployment. For example:

```console
helm install \
otxecm \
-f otxecm/platforms/<platform>.yaml \
-f otxecm/sizings/<size>.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
```

Replace `<size>` with one of the desired size options available in the folder.
The existing values are only examples and can be changed as needed.

### Install with Oracle Database

#### _Important: Extended ECM (otxecm image) is the only component that support Oracle database. The remaining components will require a PostgreSQL database_

#### Extending the base image

To use an Oracle database, the base Extended ECM docker image needs to be extended. As an example, Dockerfile_extend_oracle is provided.

To extend the image, go to the folder where the Dockerfile_extend_oracle is located and run:\
`docker build -f Dockerfile_extend_oracle -t DESIRED_IMAGE_NAME:TAG --build-arg base_image=OTXECM_IMAGE:TAG .`

#### Deploying with an Oracle Database

There are a few parameters that need to be set to deploy with an Oracle database

Set the database type to oracle:\
`--set otcs.config.database.type=oracle`

Set the hostname and port to the values for the Oracle database:\
`--set otcs.config.database.hostname=example.com`\
`--set otcs.config.database.port=1521`

When using Oracle, the service name must be set to the service name of the pluggable database to be used:\
`--set otcs.config.database.oracle.serviceName=CS`

When setting the admin user do not use the sys user. Instead, the system user must be used:\
`--set otcs.config.database.adminUsername=system`

The path for the database file and its size must be set. Its path must exist on the system where the database is running, but the .dbf file cannot:\
`--set otcs.config.database.oracle.dbDataFileSpec=/opt/oracle/cs.dbf`\
`--set otcs.config.database.oracle.dbDataFileSize=100`

#### Using a Tnsnames file

If using a tnsnames.ora file to connect to the database, the parameter must be enabled and the file must be added to the otcs subchart folder:\
`--set otcs.config.database.oracle.loadTnsnames.enabled=true`\
`--set otcs.config.database.oracle.loadTnsnames.filename=tnsnames.ora`

The connection alias of the connection to be used in the tnsnames.ora file must also be set:\
`--set otcs.config.database.oracle.tnsnamesConnectionAlias=ORCL`
For example, in the provided tnsnames.ora file the tnsnamesConnectionAlias would be ORCL.

Note: the hostname, port, and service name will not need to be set in your deployment when using a tnsnames.ora file.

#### Deploying with Oracle Data Guard
Both primary and secondary instances must be setup before the deployment.\
The configured instances need to be passed through tnsnames.ora file\
For more details refer oracle dataguard documentation.

#### Deploying custom modules/language packs using Init Containers:
To install custom modules/language packs, it is necessary to build a Docker Init container.<br>
Please, refer to the Extended ECM Cloud Deployment Guide published at OpenText My Support for details on how to build a Docker Init container for your custom module/language pack.

There are a few parameters that need to be set before deployment<br>
Set extensions enabled to true <br>
`--set otcs.config.extensions.enabled=true`<br>

If the containers inside kubernetes cluster don't have access to the internet then set includeManifestInitContainer value to true and provide init container details of manifest file as shown below<br>
`--set otcs.config.extensions.includeManifestInitContainer=true`<br>

> **Note**: Steps for building manifest file init container are provided in the [Cloud Deployment Guide](https://webapp.opentext.com/piroot/sulccd/v220300/sulccd-igd/en/html/_manual.htm) on My Support

Repeat the below 4 lines for each init container image, by incrementing index of initContainers

```console
--set otcs.initContainers[0].name='DESIRED_NAME_FOR_INIT_CONTAINER' \
--set otcs.initContainers[0].image.source='IMAGE_SOURCE' \
--set otcs.initContainers[0].image.name='IMAGE_NAME' \
--set otcs.initContainers[0].image.tag='IMAGE_TAG'
```

#### Add custom labels to the otcs pvc's
To add labels for otcs csPersist pvc, use the below helm parameter,
```console
--set otcs.pvc.csPersist.labels[0]="<sample-label-key>: <sample-label-value>" \
```
To add labels for otcs cs-logs pvc, use the below helm parameter,
```console
--set otcs.pvc.logs.labels[0]="<sample-label-key>: <sample-label-value>" \
```
To add the labels for otcs sftp-volume, use the below helm parameter,
```console
--set otcs.pvc.sftpVolume.labels[0]="<sample-label-key>: <sample-label-value>" \
```

#### Add custom annotations to the otcs services
To add annotations for otcs admin service, use the below helm parameter,
```console
--set otcs.service.admin.annotations[0]="<sample-annotation-key>: <sample-annotation-value>" \
```
To add annotations for otcs frontend service, use the below helm parameter,
```console
--set otcs.service.frontend.annotations[0]="<sample-annotation-key>: <sample-annotation-value>"  \
```
To add annotations for otcs backendSearch service, use the below helm parameter,
```console
--set otcs.service.backendSearch.annotations[0]="<sample-annotation-key>: <sample-annotation-value>"  \
```

#### Add custom annotations to the otpd service
To add annotations for otpd service, use the below helm parameter,
```console
--set otpd.service.annotations[0]="<sample-annotation-key>: <sample-annotation-value>" \
```

#### Add custom annotations to the otac service
To add annotations for otac service, use the below helm parameter,
```console
--set otac.service.annotations[0]="<sample-annotation-key>: <sample-annotation-value>" \
```

#### Add Custom labels to the otcs pods
To add labels for otcs pods, use the below helm parameter,
```console
--set otcs.podLabels.app\\.kubernetes\\.io/app_name=<app_name> \
--set otcs.podLabels.app\\.kubernetes\\.io/app_version='<app_version>' \
```

#### Deploying Transport Packages
To deploy transport packages "deployTransportPackage" parameter must be set to true (it is false by default).
`--set otcs.config.deployTransportPackage=true`
Next to that we need to provide the list of package url's that need to be deployed into CS. Make sure that the url's accessible(URL's need to be public) if not packages will not deploy.
```console
--set otcs.config.transportPackagesUrlList[0]='<url>' \
--set otcs.config.transportPackagesUrlList[1]='<url>' \
--set otcs.config.transportPackagesUrlList[2]='<url>' \
```
you can add N number of URL's list by increasing the index count.
In case of any dependencies the packages will not deploy into CS, we must deploy it manually by resolving the dependencies.
If the URL's are duplicated the packages are deployed only once.

#### enabling IPA module
To create a PVC for IPA, please set the below parameters
`--set otcs.config.contentProtection.enabled=true `
`--set otcs.config.contentProtection.storage=1Gi `
`--set otcs.config.contentProtection.path=<path of the volume> `

#### Install CSAPPS
To install CSAPPS, it is necessary to build a Docker Init container.<br>
Please, refer to the Extended ECM Cloud Deployment Guide published at OpenText My Support for details on how to build a Docker Init container for CSAPPS.<br>

To install only default apps that comes from content server, please set defaultAppsInstall as true (it is false by default)<br>
`--set otcs.config.defaultAppsInstall=true`,<br>

To install default apps that comes from content server and apps you built on your own, set defaultAppsInstall as true and
add the below 4 lines for init container image<br>

```console
--set otcs.config.defaultAppsInstall=true \
--set otcs.initContainers[0].name='DESIRED_NAME_FOR_INIT_CONTAINER' \
--set otcs.initContainers[0].image.source='IMAGE_SOURCE' \
--set otcs.initContainers[0].image.name='IMAGE_NAME' \
--set otcs.initContainers[0].image.tag='IMAGE_TAG'
```

To install the apps you built on your own, add the below 4 lines for init container image<br>

```console
--set otcs.initContainers[0].name='DESIRED_NAME_FOR_INIT_CONTAINER' \
--set otcs.initContainers[0].image.source='IMAGE_SOURCE' \
--set otcs.initContainers[0].image.name='IMAGE_NAME' \
--set otcs.initContainers[0].image.tag='IMAGE_TAG'
```

#### Upgrade CSAPPS
To upgrade CSAPPS, it is necessary to build a Docker Init container.<br>
Please, refer to the Extended ECM Cloud Deployment Guide published at OpenText My Support for details on how to build a Docker Init container for CSAPPS.<br>

To upgrade only default apps that comes from content server, please set defaultAppsUpgrade as true (it is false by default)<br>
`--set otcs.config.defaultAppsUpgrade=true`,<br>

To upgrade default apps that comes from content server and apps you built on your own, set defaultAppsUpgrade as true and
add the below 4 lines for init container image<br>

```console
--set otcs.config.defaultAppsUpgrade=true \
--set otcs.initContainers[0].name='DESIRED_NAME_FOR_INIT_CONTAINER' \
--set otcs.initContainers[0].image.source='IMAGE_SOURCE' \
--set otcs.initContainers[0].image.name='IMAGE_NAME' \
--set otcs.initContainers[0].image.tag='IMAGE_TAG'
```

To upgrade the apps you built on your own, add the below 4 lines for init container image<br>

```console
--set otcs.initContainers[0].name='DESIRED_NAME_FOR_INIT_CONTAINER' \
--set otcs.initContainers[0].image.source='IMAGE_SOURCE' \
--set otcs.initContainers[0].image.name='IMAGE_NAME' \
--set otcs.initContainers[0].image.tag='IMAGE_TAG'
```

####  To configure Syndication feature in Content Server
To Configure Syndication "otcs.config.syndication.enabled" parameter must be set to true (it is false by default).
```console
--set otcs.config.syndication.enabled='true' \

To configure primary Content Server instance "otcs.config.syndication.isPrimary" parameter must be set to true (it is false by default)
--set otcs.config.config.syndication.isPrimary='true' \(set it to false for remote server)
--set otcs.config.config.syndication.siteid='' \ (set it 0 when configuring primary, set it to other than 0 when configuring remote)
--set otcs.config.config.syndication.sitename='<sitename>' \
--set otcs.config.config.syndication.qdsUrl='<siteid>' \
--set otcs.config.config.syndication.port='<port>' \
```

### Enabling object importer in otxecm
Documents can be ingested into Content Server by using the Object Import feature
To enable object importer in content server please enable below helm paratmeter
```console
--set otcs.objectimporter.enabled=true
```
### Applying default adminsettings
To apply adminSettings only in the fresh, place the xml files in the otcs/adminSettings/initial folder. To apply adminSettings in both fresh and upgrade, place the xml files in the otcs/adminSettings/recurrent folder. Please set the below parameter to true to apply the admin settings
```console
-- set otcs.loadAdminSettings.enabled=true
```
### Applying custom path adminsettings
To apply admin settings only in fresh using custom path,create any folder in the otcs folder like otcs/<custompath>/initial.
To apply admin settings in both fresh and upgrade using custom path, create any folder in the otcs folder like otcs/<custompath>/recurrent
Please set the below parameter to the location of adminsettings folder
```console
--set otcs.adminSettingsFolder='<custom path>'(Ex: test/adminSettings) (path should be after otcs subchart folder and before initial/recurrent )
-- set otcs.loadAdminSettings.enabled=true
```

### Using existing helm assets

#### <b> Admin settings configmaps </b>
To apply externalized admin settings first create the configmaps for the recurrent or initial admin settings.
```console
kubectl create configmap <initial/recurrent-configmap-name> --from-file=<path to adminsettings xml>
```
Then when deploying add the parameter that corresponds with your configmap
```console
--set otcs.loadAdminSettings.enabled=true \
--set loadAdminSettings.initialConfigmap=<initial-configmap-name>
```
or
```console
--set otcs.loadAdminSettings.enabled=true \
--set loadAdminSettings.recurrentConfigmap=<recurrent-configmap-name>
```

#### <b>OTAC/OTACC certificate secret</b>
To use an existing secret for the otac or otacc certificate create a secret as such
```console
kc create secret generic <certificate-secret-name> --from-file=<PATH-TO-CERTIFICATE>
```
Then add the corresponding set command to the helm deploy
```console
--set otcs.config.otac.certSecret=<certificate-secret-name> \
--set otcs.config.otac.certFilename=<certificate-filename>
```
or
```console
--set otcs.config.otacc.certSecret=<certificate-secret-name> \
--set otcs.config.otacc.certFilename=<certificate-filename>
```

#### <b>OTXECM license secrets</b>
To use an existing secret for the otxecm licenses create a secret as such
```console
kc create secret generic <license-secret-name> --from-file=<PATH-TO-LICENSE>
```
Then add the corresponding set command to the helm deploy
```console
--set global.existingLicenseSecret=<license-secret-name> \
--set otcs.loadLicense.enabled=true \
--set otcs.loadLicense.filename=<otcs-license-filename>
```

### Scrape prometheus metrics of adminserver processes:
If you want to scrape metrics of admin server processes using prometheus, set below parameters
```console
--set otcs.config.prometheusMetrics.enabled=true
--set otcs.config.prometheusMetrics.key=<key>
```
If your prometheus podmonitor selector is looking for specific labels, then update your podmonitor selector to match for below label.
```console
monitor: adminserver
```
You can add custom labels for the otcs-admin-podmonitor using below option
```console
--set otcs.adminPodMonitor.labels.<key>=value
```
> **Note**: we make use of the prometheus operator and that the CRDs need to be installed in order to work properly

[Prometheus monitoring stack installation using Prometheus Operator](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

### Enabling Fluentbit log outputs
Content Server security  or system monitoring logs can be outputed to fluentbit

To enable the fluentbit container, you need to add the following set command

```console
--set otcs.fluentbit.enabled=true
```

The command to enable system monitoring logs is:
```console
--set otcs.fluentbit.logsToMonitor[0]="sysmon" \
--set otcs.config.enableSysmonLogs=true
```
The command to enable security logs is:
```console
--set otcs.fluentbit.logsToMonitor[0]="security" \
--set otcs.config.enableSecurityLogs=true
```

>**Note**: logsToMonitor is an array and will need to be incremented if both security and system monitoring logs are enabled

When the fluentbit container is enabled, it will be running in the same pod as the otcs pods. To view the fluentbit output you need to run the following command:
```console
kubectl logs [-f] <otcs-pod-name> -c fluentbit-container
```
Alternatively, to view the log file of the otcs container

```console
kubectl logs [-f] <otcs-pod-name> -c otcs-<admin,frontend,backend-search>-container
```

## Upgrading the Chart

Upgrade deployment with Helm:

```console
helm list
helm upgrade <RELEASE_NAME> otxecm
```

Replace `<RELEASE_NAME>` with your Helm chart release name (shown by `helm list`).

Note that if you're upgrading from 21.3 or earlier and the Intelligent Viewing chart has also been installed, first delete the Intelligent Viewing chart (e.g., helm delete otiv) prior to upgrading to the 21.4 since the otiv chart has been added as an otxecm subchart.

## Scaling the Deployment

The Content Server frontend deployment can be scaled to cover different load levels. Per default one replica is started for the Kubernetes stateful set `otcs-frontend`. This should be done with 'helm upgrade', since the helm values are used. To scale it to 2, use your original 'helm install' command with this additional line:

```console
helm upgrade <RELEASE_NAME> otxecm \
--set otcs.contentServerFrontend.replicas=2
```
Scaling with kubectl is also supported using the below command
```console
kubectl scale sts otcs-frontend --replicas='no of replicas'
```
Content Server admin (search) instances can be scaled up, but not down. Similar to frontends, use your original 'helm install' command with this additional line:

```console
helm upgrade <RELEASE_NAME> otxecm \
--set otcs.contentServerBackendSearch.replicas=2
```
Scaling with kubectl is also supported using the below command
```console
kubectl scale sts otcs-backendsearch --replicas='no of replicas'
```
When scaling is done using kubectl, user needs to pass replica count again while upgrading the otxecm because helm does not recognize the kubectl scaling.
## Uninstalling the Chart

To check the name of existing charts:

```console
helm ls
```

To completely uninstall/delete the `my-release` deployment including all persisted data:

```console
helm delete my-release
kubectl delete pvc --all
```
To delete an existing otxecm-default-secrets use the following command

```console
kubectl delete secret otxecm-default-secrets
```
If OTAC was deployed remove configMap and job related to otac upgrade

```console
kubectl delete configmap otac-pre-upgrade-configmap

kubectl delete jobs otac-pre-upgrade-job
```

If OTIV was deployed remove all secrets and service accounts related to otiv

```console
kubectl delete sa otiv-job-sa otiv-pvc-sa

kubectl delete secret otiv-cs-secrets otiv-highlight-secrets otiv-job-sa-token otiv-publication-secrets otiv-publisher-secrets otiv-pvc-sa-token otiv-resource-secret
```

> **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

Basic configuration can be done in the `platforms` directory inside the Helm chart. There you find dedicated YAML files for each platform:

- `gcp.yaml` (for [Google Cloud Platform GKE](#example-installation-and-configuration-for-google-cloud-platform))
- `azure.yaml` (for Microsoft [Azure AKS](#example-installation-and-configuration-for-azure-aks))
- `aws.yaml` (for [Amazon AWS EKS](#example-installation-and-configuration-for-amazon-aws))
- `cfcr.yaml` (for Cloud Foundry Container Runtime)
- `minikube.yaml` (for [Minikube](#example-installation-and-configuration-for-minikube) on your local computer - this is not meant for productive use)
- `openshift.yaml` (for [RedHat OpenShift](#example-installation-and-configuration-for-redhat-code-ready-container))

Most importantly you have to adjust the following settings in these platform YAML files:

- Docker image repository
- enable or disable the use of Fully Qualified domain names (this requires to set the `ingressEnabled` to `true`)
- enable the use of SSL / HTTPS (this also requires to set the `ingressEnabled` to `true` and to specify the name of the Kubernetes secret: `ingressSSLSecret`)

For more advanced settings you can also review the `values.yaml` file in the Helm chart directory to adjust parameters for Extended ECM (Content Server) and its components (e.g., Archive Center, Directory Services, etc). The better alternative may be to pass changed values with the help of the `--set` option in the `helm install` command (and not modify the `values.yaml` directly).

Some examples:

```console
helm install \
otxecm \
--set otcs.image.name=otxecm\
-f otxecm/platforms/gcp.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='

helm install \
otxecm \
--set global.otac.enabled=false \
--set otcs.config.documentStorage.type=database \
-f otxecm/platforms/gcp.yaml otxecm \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='

helm install \
otxecm \
--set otcs.image.name=otxecm-sap-o365-sfdc \
--set otcs.contentServerFrontend.replicas=2 \
-f otxecm/platforms/gcp.yaml  \
--set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
```

### Example Installation and Configuration for Google Cloud Platform

This example creates a cluster named `xecm-cluster` and uses SSL (HTTPS). You need to create the `fullchain.pem` certificate file and the private key file `privkey.pem` before (e.g., with certbot and Let's encrypt). Also you need to create a container registry in GCP and push the Extended ECM Docker image and its components ( e.g., Archive Center, Directory Services, Intelligent Viewing).

1. Login to GCP and set GCP Project and Compute/Zone

    ```console
    gcloud auth login --no-launch-browser
    gcloud config set project <YOUR PROJECT ID>
    gcloud config set compute/zone <YOUR COMPUTE ZONE>
    ```

1. Create Cluster

    ```console
    gcloud container clusters create xecm-cluster \
    --machine-type n1-standard-4 \
    --num-nodes 3 \
    --cluster-version <set-a-supported-version> \
    --enable-stackdriver-kubernetes \
    --enable-ip-alias
    ```

1. Configure `kubectl` for the created cluster

    ```console
    gcloud container clusters get-credentials xecm-cluster --zone <YOUR COMPUTE ZONE> --project <YOUR PROJECT ID>
    ```

1. Create a static IP address in GCP

    ```console
    gcloud compute addresses create xecm-ip --region <YOUR COMPUTE ZONE>
    ```

1. Create DNS Entries

    Now we register a DNS zone in GCP and create three records for Extended ECM, Archive Server and Directory Services that will all point to the static IP adress you have created in GCP in the step before (if you deploy without Archive Center you don't need a DNS record for it).

    Replace `xecm-cloud.com` with your registered Internet domain (DNS name). Also replace the Internet addresses below with the static IP you created before.

    ```console
    gcloud dns managed-zones create xecm-cloud \
    --dns-name="xecm-cloud.com" \
    --description="DNS Zone for Extended ECM Deployment" \
    --visibility=public

    gcloud dns record-sets transaction start --zone="xecm-cloud"

    gcloud dns record-sets transaction add 10.2.3.4 \
    --name="otac.xecm-cloud.com" \
    --ttl="5" \
    --type="A" \
    --zone="xecm-cloud"
    gcloud dns record-sets transaction add 10.2.3.4 \
    --name="otcs.xecm-cloud.com" \
    --ttl="5" \
    --type="A" \
    --zone="xecm-cloud"
    gcloud dns record-sets transaction add 10.2.3.4 \
    --name="otds.xecm-cloud.com" \
    --ttl="5" \
    --type="A" \
    --zone="xecm-cloud"

    gcloud dns record-sets transaction execute --zone="xecm-cloud"
    ```

1. Prepare Helm Chart Deployment

    ```console
    kubectl create secret tls xecm-secret --cert fullchain.pem --key privkey.pem
    ```

1. Deploy Ingress Controller

    You need to replace the IP address with the one you created before. The `proxy-body-size` parameter controls the maximum allowable request size. You may need to increase it to upload larger files.

    ```console
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm install otxecm ingress-nginx/ingress-nginx \
    --set rbac.create=true \
    --set controller.service.loadBalancerIP=<PUBLIC FACING IP> \
    --set controller.config.proxy-body-size=1024m
    ```

1. Deploy Helm Chart

    ```console
    helm install otxecm -f otxecm/platforms/gcp.yaml otxecm \
    --set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
    ```

Access Directory Services and Content Server frontends with these URLs:

- Content Server: `https://otcs.<DOMAIN_NAME>/cs/cs`
- Directory Services: `https://otds.<DOMAIN_NAME>/otds-admin`

To shutdown the deployment and delete the cluster do the following (replace the `<RELEASE_NAME>` with the release names `helm list` returns):

```console
helm list
helm delete <RELEASE_NAME>
helm delete otxecm
kubectl delete pvc --all
gcloud container clusters delete xecm-cluster --zone <YOUR COMPUTE ZONE>
gcloud compute addresses delete xecm-ip --region <YOUR COMPUTE ZONE>
```

> **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

### Example Installation and Configuration for Amazon AWS

This example creates a cluster named <AWS_CLUSTER_NAME> and uses TLS (SSL). You need to create the TLS certificate on the AWS web portal using the `Certificate Manager` service. You need to create a docker container registry either on AWS or elsewhere that is accessible to AWS. Make sure the docker images you are using are pushed and available.

1. Create Cluster

    ```console
    eksctl create cluster \
    --name <AWS_CLUSTER_NAME> \
    --version <set-a-supported-version> \
    --region <YOUR COMPUTE ZONE> \
    --nodegroup-name <AWS_CLUSTER_NAME>-workers \
    --node-type t3.xlarge \
    --nodes 3 \
    --nodes-min 1 \
    --nodes-max 4 \
    --alb-ingress-access \
    --external-dns-access \
    --full-ecr-access \
    --managed
    ```

1. Configure `kubectl` for the created cluster

    If not done before: set AWS Credentials (you need AWS Access Key ID, and AWS Secret Access Key for this):

    ```console
    aws configure
    ```

    If you have not created the cluster with `eksctl` on the same computer before you need to manually create a kubeconfig entry:

    ```console
    aws eks --region <AWS_REGION> update-kubeconfig --name <AWS_CLUSTER_NAME>
    ```

    Check that your new cluster is registered with `kubectl`:

    ```console
    kubectl config get-contexts
    ```

    If you have multiple contexts for your local `kubectl` you may need to switch to the one for your AWS cluster:

    ```console
    kubectl config use-context <AWS_CLUSTER_NAME>
    ```

    Check if kubectl can communicate with the Kubernetes Cluster in AWS:

    ```console
    kubectl version
    ```

1. Prepare Helm Chart Deployment

    Switch to the directory that includes your certificate files.

    ```console
    kubectl create secret tls <AWS_CLUSTER_NAME>-secret --cert fullchain.pem --key privkey.pem
    ```

1. Deploy Ingress Controller

    The AWS Load Balancer Controller manages AWS Elastic Load Balancers for a Kubernetes cluster

    [Installing the AWS Load Balancer Controller](https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html)


1. Create EBS CSI driver IAM role for service accounts(Required for EKS 1.23 and above)


    The Amazon EBS CSI plugin requires IAM permissions to make calls to AWS APIs on your behalf.

    [Creating the Amazon EBS CSI driver IAM role for service accounts](https://docs.aws.amazon.com/eks/latest/userguide/csi-iam-role.html)

1. Deploy Helm Chart

    Now we can install the `otxecm` Helm chart:

    ```console
    helm install otxecm otxecm -f otxecm/platforms/aws.yaml \
    --set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
    ```

   The deployment of the Helm Chart triggers the creation of an ALB loadbalancer for the Kubernetes Ingress.

1. Configure DNS

    At this point you have to go to [AWS Route 53 DNS zone management](https://console.aws.amazon.com/route53/home) and create the DNS entries for OTDS, OTCS and OTAC and point them to the created load balancer (Alias)

    You can list hosted zones with this command:

    ```console
    aws route53 list-hosted-zones

    aws route53 list-resource-record-sets --hosted-zone-id <ZONE_ID>
    ```

    Replace `<ZONE_ID>` with the zone ID `aws route53 list-hosted-zones` returned.

    We need three DNS record sets:
    - OTDS (use name "otds")
    - Content Server frontend (use name "otcs")
    - Archive Center (use name "otac")

    For all choose the type "A - IPv4 address" and set "Alias" to yes. Select the ALB loadbalancer as "Alias Target".

Access Directory Services and Content Server frontends with these URLs:

- Content Server: `https://otcs.<DOMAIN_NAME>/cs/cs`
- Directory Services: `https://otds.<DOMAIN_NAME>/otds-admin`

To shutdown the deployment follow these steps:

1. Remove the DNS entries that have aliases to the load balancer

    > **Important**: To shutdown the deployment you first have to remove the Aliases in the DNS records in AWS Route 53. Go to AWS Route 53 and delete the Record Sets for otac, otcs, and otds you have created during deployment.

1. Delete the otxecm Helm chart

    > **Important**: delete first the otxecm Helm Chart - then the ALB Helm Chart - otherwise the ALB loadbalancer will not be deleted and you will run into issues if you delete the cluster.

    You can delete the otxecm Helm Chart like this (replace `<RELEASE_NAME>` with the release name `helm list` returns):

    ```console
    helm list

    helm delete <RELEASE_NAME>
    ```

1. Delete persistent storage (if you want to start from scratch)

    ```console
    kubectl delete pvc --all
    ```

    > **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

1. Delete the ALB Helm Chart

    You can delete the ALB Helm Chart like this (replace `<ALB_RELEASE_NAME>` with the release name `helm list` returns):

    ```console
    helm list -n kube-system

    helm delete -n kube-system <ALB_RELEASE_NAME>
    ```

1. Delete the Kubernetes Cluster

    > **Important**: Remove everything you have created manually in AWS as this otherwise will not be deleted by `eksctl` and thus the deletion of the cluster will fail and you will end up with stale AWS resources you have to delete manually!
    > **Important**: Check if all Kubernetes resources are really freed up before deleting the cluster: `kubectl get pv` and `kubectl get pvc` should not list any resources. In doubt wait some time.

    Then you can delete the cluster with this command:

    ```console
    eksctl delete cluster --name <AWS_CLUSTER_NAME> --region <AWS_REGION> --wait
    ```

Using EFS (external file system):

```console
aws efs create-file-system \
--creation-token <AWS_CLUSTER_NAME>  \
--performance-mode generalPurpose \
--throughput-mode bursting \
--region <AWS_REGION>
```

Take note of the file system ID that is output by the command above.

Then install Amazon EFS CSI driver:

The Amazon EFS Container Storage Interface (CSI) driver provides a CSI interface that allows Kubernetes clusters running on AWS to manage the lifecycle of Amazon EFS file systems.

[Installing the Amazon EFS CSI driver](https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html)

Check that a new Kubernetes storage class `aws-efs` has been created:

```console
kubectl get storageclasses
```

The storage class `aws-efs` should be listed.

To delete the external file system:

```console
aws efs delete-file-system --file-system-id <YOUR FILE SYSTEM ID>
```

### Example Installation and Configuration for Azure AKS

This example creates a cluster named `xecm-cluster` and uses TLS (SSL). You need to create the TLS certificate with Let's Encrypt, or some other method that makes available fullchain.pem and privatekey.pem files. You need to create a docker container registry either on Azure or elsewhere that is accessible to Azure. Make sure the docker images you are using are pushed and available.

1. Create Resource Group

    Replace `myresourcegroup` with your own resource group name and `westeurope` with your preferred Azure location.

    ```console
    az group create --name myresourcegroup --location westeurope
    ```

1. Create Cluster

    Alternately, you can create a kubernetes cluster with the Azure web portal.

    Replace `myresourcegroup` with the resource group name your created before and `westeurope` with your preferred Azure location. Also replace `myregistry` with the name of your container registry in Azure.

    ```console
    az aks create \
    --name xecm-cluster \
    --resource-group myresourcegroup \
    --kubernetes-version <set-a-supported-version> \
    --node-count 3 \
    --node-vm-size Standard_B4ms \
    --attach-acr myregistry \
    --dns-name-prefix xecm \
    --location westeurope
    ```

1. Configure `kubectl` for the created cluster

    Replace `xecm-cluster` with the name of the cluster you created in the step before and `myresourcegroup` with the resource group name your created before.

    ```console
    az aks get-credentials \
    --name xecm-cluster \
    --resource-group myresourcegroup
    ```

1. Connect Azure Kubernetes cluster with the Azure container repository

    ```console
    az aks update \
    --name xecm-cluster \
    --resource-group myresourcegroup \
    --attach-acr extendedecm
    ```

1. Create a static IP address in Azure

    Replace `xecm-cluster` with the name of the cluster you created in the step before and `myresourcegroup` with the resource group name your created before. After the IP is created, you will need to point your dns for any domains being used to this IP.

    ```console
    az network public-ip create \
    --name xecm-ip \
    --resource-group MC_myresourcegroup_xecm-cluster_westeurope \
    --allocation-method static \
    --query publicIp.ipAddress \
    --sku Standard \
    -o tsv
    ```

1. Prepare Helm Chart Deployment

    Create a kubernetes secret from your TLS certificate files.

    ```console
    kubectl create secret tls xecm-secret --cert fullchain.pem --key privkey.pem
    ```

1. (Optional) Create a custom storage class if you are using shared storage across multiple Content Server Admin (search) servers.

    The Content Server Admin server requires the ability to change file timestamps. A custom storage class must be created to allow the mounted files to be owned by the user running the container. The following yaml file is only an example, and may be out of date for syntax. You may also need to modify the 'skuName' if you have different storage requirements. Please see this url for full options:

    [Azure - Create a custom storage class](https://docs.microsoft.com/en-us/azure/aks/azure-files-csi#create-a-custom-storage-class)

    ```yaml
    kind: StorageClass
    apiVersion: storage.k8s.io/v1
    metadata:
      name: shared-azurefile
    provisioner: file.csi.azure.com
    reclaimPolicy: Delete
    volumeBindingMode: Immediate
    allowVolumeExpansion: true
    mountOptions:
      - dir_mode=0777
      - file_mode=0777
      - uid=1000
      - gid=1000
      - mfsymlinks
    parameters:
      skuName: Standard_LRS
    ```

    Copy the above yaml to a filename of your choice and create the storage class:

    ```console
    kubectl create -f my_storage_class.yaml
    ```

    When deploying the helm chart below, you must specify this parameter to use your storage class:

    ```console
    --set otcs.config.search.sharedSearch.storageClassName=shared-azurefile
    ```

1. Deploy Ingress Controller

    Replace the IP address with the one created previously.

    ```console
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm install otxecm-ingress ingress-nginx/ingress-nginx \
    --set rbac.create=true \
    --set controller.service.loadBalancerIP=<YOUR STATIC IP ADDRESS>
    ```

1. Deploy Helm Chart

    Edit the otxecm/platforms/azure.yaml file and update the imageSource to match your container registry. Edit your public hostnames to match what you are using.

    ```console
    helm install otxecm -f otxecm/platforms/azure.yaml otxecm \
    --set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
    ```

Access Directory Services and Content Server frontends with these URLs:

- Content Server: `https://<OTCS_DOMAIN_NAME>/cs/cs`
- Directory Services: `https://<OTDS_DOMAIN_NAME>/otds-admin`

To shutdown the deployment and delete the cluster do the following (replace the `<RELEASE_NAME>` with the release names `helm list` returns):

```console
helm list
helm delete <RELEASE_NAME>
helm delete otxecm-nginx
kubectl delete pvc --all
az aks delete --name xecm-cluster --resource-group myresourcegroup
```

> **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

### Example Installation and Configuration for Minikube (for NON-production use)

This example creates a minikube 1-node cluster and pulls images from Docker Hub with the minikube internal docker installation.
You have to replace the "DOCKER_" placeholders with your docker hub credentials.

1. Create Cluster

    On initial start provide the memory and CPU requirements:

    ```console
    minikube start --memory 16384 --cpus 4
    minikube status
    ```

    On follow-up starts you don't need to specify memory and CPU - it is remembered by Minikube:

    ```console
    minikube start
    ```

1. Create Kubernetes secret for Docker registry

    ```console
    kubectl create secret docker-registry regcred \
    --docker-server=https://index.docker.io/v1/ \
    --docker-username="<DOCKER_ACCOUNT>" \
    --docker-password="<DOCKER_PASSWORD>" \
    --docker-email=<DOCKER_EMAIL>
    ```

1. Create a tunnel to the local computer / browser

    Start a second shell and tunnel the Kubernetes services to the local computer.

    ```console
    minikube tunnel
    ```

1. Deploy Helm Chart

    Then you can install the helm chart in the first shell (using the database as a document storage type):

    ```console
    helm install otxecm otxecm \
    --set otcs.image.name=otxecm \
    --set global.otac.enabled=false \
    --set otcs.config.documentStorage.type=database \
    -f otxecm/platforms/minikube.yaml \
    --set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='
    ```

    Wait until the pods are running and the services are available:

    ```console
    kubectl get pods -w
    kubectl get service -w
    ```

You can lookup the endpoints of the Content Server frontends and PowerDocs in the column "EXTERNAL-IP"
and add the port that is listed in the "PORT(S)" column (the number in front of the colon)

> **Tip**: You have to add `/otds-admin` to the Directory Services URL and `/cs/cs` to the Content Server URL.

To shutdown the deployment and delete the cluster do the following (replace the `<RELEASE_NAME>` with the release name `helm list` returns):

```console
helm list
helm delete <RELEASE_NAME>
kubectl delete pvc --all
minikube tunnel --cleanup=true
minikube stop
minikube delete
```

> **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

### Example Installation and Configuration for RedHat Code Ready Container (for NON-production use)

Download CRC and the pull secret from the [RedHat CRC website](https://cloud.redhat.com/openshift/install/crc/installer-provisioned).

You may need to create a (free) account to get access.

1. Check to Code Ready Container (crc) is properly installed

    ```console
    crc version
    ```

1. Setup and Start crc

    Depending on the size of your machine you can tweak the parameters for memory (`-m`) and number of CPUs (`-c`).

    ```console
    crc setup

    crc start -c 6 -m 32768 -p ~/Downloads/pull-secret
    ```

    > **Important**: Take note of the output of the `crc start` command - it shows you the **admin password** for the cluster you need in the next steps.

1. Setup OpenShift and login

    You need to provide the admin user with `-u` and the admin password with `-p`.

    ```console
    eval $(crc oc-env)

    oc login -u kubeadmin -p <YOUR ADMIN PASSWORD> https://api.crc.testing:6443
     ```

    To check versions and see which nodes got created you can use these commands (this is optional):

    ```console
    oc version

    kubectl cluster-info

    kubectl get nodes
    ```

1. Setup OpenShift Project & Policies

    Run your deployment in an own OpenShift project and use `otxecm` as the name of this project. This will also create a Kubernetes namespace with that name. If you have not yet created a project named `otxecm` do so with this command:

    ```console
    oc new-project otxecm
    ```

    Otherwise switch to your existing `otxecm` project with this command:

    ```console
    oc project otxecm
    ```

    Optional you can check the content of the `otxecm` project and that the namespace have been created:

    ```console
    oc get project otxecm -o yaml

    kubectl get namespaces
    ```

    OpenShift also requires your deployment to run in its own **service account** - call it `otxecm-service-account` and use the `-n` option to define the project / namespace you created before:

    ```console
    oc create serviceaccount otxecm-service-account -n otxecm

    kubectl get serviceaccounts
    ```

    Now you have to set appropriate permissions:

    ```console
    oc adm policy add-scc-to-user privileged system:serviceaccount:otxecm:otxecm-service-account
    ```

1. Hack Persistent Volume Permissions

    This is a dirty hack to avoid permission problems with the persistent volumes CRC provides per default. This seems to be a known issue with CRC (the pre-provisioned persistent volumes are owned by root and don't give other access).

    You first have to find out the name of your Kubernetes node. Then you can open a debug session into that node to adjust the permissions of the 30 persistent volumes CRC has created at startup (`pv0001` - `pv0030`).

    ```console
    oc get nodes

    oc debug node/<node_name>

    cd /host/mnt/pv-data

    chmod 777 pv*

    exit
    ```

1. Start CRC Console

    ```console
    crc console
    ```

    Use these login information
    - Login: kubeadmin
    - Password: \<YOUR ADMIN PASSWORD>

    Select Home --> Projects in the menu on the left and select "otxecm".

1. Extended ECM Deployment

    Create secret for pulling Docker images from Docker Hub:

    ```console
    kubectl create secret docker-registry regcred \
    --docker-server=https://registry.opentext.com/v2/ \
    --docker-username="<OT_ACCOUNT>" \
    --docker-password="<OT_PASSWORD>" \
    --docker-email=<EMAIL>
    ```

    Deploy Helm Chart:

    ```console
    helm install otxecm -f otxecm/platforms/openshift.yaml otxecm \
    --set otds.otdsws.cryptKey='Z2hkN2hyNDBkbWNGcVQ0TA=='

    watch kubectl get pods
    ```

1. Expose Kubernetes Services as routes in OpenShift

    ```console
    oc expose service otds

    oc expose service otcs-frontend
   ```

1. Access Directory Services and Content Server frontends

    <http://otds-otxecm.apps-crc.testing/otds-admin/>

    <http://otcs-frontend-otxecm.apps-crc.testing/cs/cs?func=llworkspace>

    <http://otcs-frontend-otxecm.apps-crc.testing/cs/cs/app>

1. Stop and Restart CRC

    To (temporarily) stop CRC:

    ```console
    crc stop
    ```

1. Finally delete CRC

    To (finally) delete CRC and the Kubernetes cluster:

    ```console
    crc delete
    ```

    A `crc delete` does not automatically remove the kubectl configurations - you have to do this manually to fully clean things up:

    ```console
    kubectl config delete-context otxecm/api-crc-testing:6443/kube:admin

    kubectl config delete-context default/api-crc-testing:6443/kube:admin
    ```

### Example Installation and Configuration for Canonical MicroK8s (for NON-production use)

This example creates a MicroK8s 1-node cluster and pulls images from Docker Hub.
You have to replace the "DOCKER_" placeholders with your docker hub credentials.

1. Installation

    MicroK8s runs best on Linux - e.g., an up-to-date Ubuntu or Fedora Linux distribution.

    First you need to install the snap package manager. It is required to then install MicroK8s.

    On Fedora Linux:

    ```console
    sudo dnf install snapd

    sudo ln -s /var/lib/snapd/snap /snap
    ```

    On Ubuntu Linux:

    ```console
    sudo apt install snapd
    ```

    Then install MicroK8s:

    ```console
    sudo snap install microk8s --classic --edge
    ```

    You need to enable a couble of MicroK8s modules that we need for our deployment:

    ```console
    microk8s enable helm3 dns storage dashboard rbac metallb
    ```

    During enabling of `metallb` you may be asked for an IP address range. Just confirm the proposed range:

    ```console
    Enter the IP address range (e.g., 10.64.140.43-10.64.140.49): 10.64.140.43-10.64.140.49
    ```

1. Create Cluster

    On initial start provide the memory and CPU requirements:

    ```console
    microk8s start

    microk8s status

    microk8s kubectl version
    ```

1. Create Kubernetes secret for Docker registry

    ```console
    microk8s kubectl create secret docker-registry regcred \
    --docker-server=https://index.docker.io/v1/ \
    --docker-username="<DOCKER_ACCOUNT>" \
    --docker-password="<DOCKER_PASSWORD>" \
    --docker-email=<DOCKER_EMAIL>
    ```

1. Deploy Helm Chart

    Then you can install the helm chart in the first shell (using the database as a document storage type):

    ```console
    microk8s helm3 install otxecm otxecm \
    --set otcs.image.name=otxecm \
    -f otxecm/platforms/microk8s.yaml \
    ```

    Wait until the pods are running and the services are available:

    ```console
    microk8s kubectl get pods -w
    microk8s kubectl get service -w
    ```

    > ***Important***: the load balancer services need an EXTERNAL-IP assigned. Otherwise you may not have enabled the `metallb` in the previous steps.

You can access Content Server frontends and Directory Services with these commands:

```console
http://<EXTERNAL_IP_OF_OTCS_FRONTEND>/cs/cs/app
http://<EXTERNAL_IP_OF_OTDS>/otds-admin
```

> **Tip**: You have to add `/otds-admin` to the OpenText Directory Services URL that is opened by minikube service otds.

You can also access the Kubernetes Dashboard

```console
microk8s kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443
```

Then you can then access the Dashboard at `https://127.0.0.1:10443`
You will be asked for a token that you can get by these commands:

```console
token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)

microk8s kubectl -n kube-system describe secret $token
```

Copy the token into the browser page.

To shutdown the deployment and delete the cluster do the following (replace the `<RELEASE_NAME>` with the release name `helm list` returns):

```console
microk8s helm3 list
microk8s helm3 delete <RELEASE_NAME>
microk8s kubectl delete pvc --all
microk8s stop
```

> **Important**: `kubectl delete pvc --all` will delete all the persistent data of your deployment (including database storage). Do this only if you want to start from scratch!

To completely startover you can also reset your node / deployment.

```console
microk8s reset
```
