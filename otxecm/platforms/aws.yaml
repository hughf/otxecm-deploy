########################################################################
# Platform specific settings for AWS (Amazon Web Services) Platform
# See otxecm/values.yaml for detailed description of these values.
########################################################################

                                          # CHANGE HERE ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
global:
  ingressClass:                                         alb
  ingressAnnotations:
    alb.ingress.kubernetes.io/scheme:                   internet-facing
    alb.ingress.kubernetes.io/certificate-arn:          <YOUR  arn:aws:acm CERTIFICATE>
    alb.ingress.kubernetes.io/load-balancer-attributes: idle_timeout.timeout_seconds=1800
    alb.ingress.kubernetes.io/target-group-attributes:  stickiness.enabled=true,stickiness.lb_cookie.duration_seconds=30
    alb.ingress.kubernetes.io/target-type:              ip
    alb.ingress.kubernetes.io/configuration-snippet: |
      if ($http_origin) {more_set_headers "Access-Control-Allow-Headers: $sent_http_access_control_allow_headers, X-XHR-Logon, X-Requested-With";}
  ## ingressEnabled is normally enabled for production environments. If disabled, you can either deploy using
  ## NodePort which is enabled by default or deploy using LoadBalancer
  ## which is typically assigned a CNAME or static IP, depending on the kubernetes
  ## platform. If you want to use NodePort without ingress, you must set serviceType below to NodePort
  ## and ingress to false or if you want to deploy using LoadBalancer you must set serviceType to LoadBalancer
  ## You must also set the xPublicUrl values below to an empty string "" in order to deploy Load balancer.
  ingressEnabled:                                       true
  ingressSSLSecret:                                     xecm-secret
  serviceType:                                          NodePort
  ## ingressDomainName is used to construct the ingress URLs for OTIV services
  ## The value is equal to the portion of the otcsPublicUrl value minus the
  ## https protocol and first DNS field (i.e. {otcsPublicUrl} - https://otcs.)
  ingressDomainName:                                    example.com
  otcsPublicUrl:                                        https://otcs.example.com
  otdsPublicUrl:                                        https://otds.example.com
  otacPublicUrl:                                        https://otac.example.com
  otaccPublicUrl:                                       https://otacc.example.com
  otpdPublicUrl:                                        https://otpd.example.com
  storageClassName:                                     gp2
  storageClassNameNFS:                                  nfs
  ## masterPassword: default secret i.e otxecm-default-secrets will use this value to populate required key values
  ## otxecm-default-secrets will be created only when .global.existingSecret value is "otxecm-default-secrets"
  ## This values must use the OTDS password requirements of at least 8 characters, 1 upper
  ## case, 1 lower case, 1 digit, 1 symbol.
  masterPassword:
  imageSource:                                          registry.opentext.com
  ## imageSourcePublic is the docker registry for retrieval of 3rd party docker images
  imageSourcePublic:                                    docker.io
  imagePullSecret:
  imagePullPolicy:                                      Always
  resourceRequirements:                                 true
  serviceAccountName:                                   default
  ## existingSecret: An already existing kubernetes secret, which contains passwords.
  ## If you have existing kubernetes secret replace otxecm-default-secrets with existing secret name
  ## If you don't have existing secret then specify .global.masterPassword value, a default secret with name otxecm-default-secrets 
  ## will be created by consuming secret values from masterPassword
  existingSecret:                                       otxecm-default-secrets
  ## existingLicenseSecret: An already existing kubernetes secret, which contains the otxecm license files.
  existingLicenseSecret: 
  ## timeZone: This sets the time zone of the Linux OS within the container. The default value for
  ## the timeZone is Etc/UTC. The value for the timeZone MUST be a CORRECT value from a trusted
  ## source, otherwise unexpected behaviour will occur, this could have unforeseen repercussions for
  ## software running in the container.
  ## For a list of time zones, see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
  timeZone:                                             Etc/UTC
  # values for the otds helm chart. These are also used by the otxecm parent chart.
  ## otdsUseReleaseName controls whether the release name is used in
  ## the names of the objects.
  otdsUseReleaseName:                                   false
  ## otdsServiceName defines the hostname of the Kubernetes container for
  ## OTDS (inside the stateful set definition):
  otdsServiceName:                                      otds