#!/bin/bash 
DEPLNAME=otxecm-db
DEPLSRC=oci://registry-1.docker.io/bitnamicharts/postgresql
DBPWD=l1v3Link!! 
DBIMGTAG=12
DBVERS=12.6.8  

CMDARGS=" --set global.postgresql.auth.postgresPassword=${DBPWD}"
CMDARGS+=" --set image.registry=docker.io"
CMDARGS+=" --set image.repository=postgres"
CMDARGS+=" --set image.tag=${DBIMGTAG} "
CMDARGS+=" --set fullnameOverride=otxecm-db" 
CMDARGS+=" --version=${DBVERS}" 

echo helm install ${DEPLNAME} ${DEPLSRC} ${CMDARGS} 
helm install ${DEPLNAME} ${DEPLSRC} ${CMDARGS} 
