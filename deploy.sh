#!/bin/sh
WORKDIR=`pwd`

echo Setting up secrets 
${WORKDIR}/setup-secrets.sh

echo Setting up certificate manager...
${WORKDIR}/helm-certmgr.sh

echo Setting up Ingress-nginx controller...
${WORKDIR}/helm-ingress.sh 

echo Setting up database...
${WORKDIR}/helm-otcs-db.sh

echo Setting up OTCS/OTCS using xECM helm charts...
${WORKDIR}/run-helm-with-args.sh 

