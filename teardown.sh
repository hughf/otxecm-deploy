#!/bin/sh
#Helm has deprecated delete and we should be using uninstall.
# Paramaterizing in case we need to swap for different helm versions

DELETE_CMD=uninstall
echo Undeploying all helm charts...
helm ${DELETE_CMD} myotxecm 
helm ${DELETE_CMD} otxecm-db
helm ${DELETE_CMD} otxecm-ingress
# Not sure if we need this yet
#helm ${DELETE_CMD} certmgr 

echo Cleaning up volumnes
kubectl delete pvc -all 

