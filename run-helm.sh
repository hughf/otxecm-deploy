#!/bin/bash
OTDSKEY=123456789ABCDEFG
OTDSCRYPTKEY=`echo -n ${OTDSKEY} | base64 -w 0| base64 -w 0` 
#OTDSCRYPTKEY=Z2hkN2hyNDBkbWNGcVQ0TA==
WORKDIR=`pwd`
DEPLNAME=myotxecm
DEPLDIR=otxecm
PLATFORM=gcp.yaml
FILEARGS=" -f ${WORKDIR}/${DEPLDIR}/platforms/${PLATFORM}"
CMDLINE=" --set otcs.config.documentStorage.type=database" 
CMDLINE+=" --set global.otac.enabled=false --set global.otac-db.enabled=false"  
CMDLINE+=" --set global.otcs-db.enabled=true --set otcs.config.extensions.enable=true"
CMDLINE+=" --set global.otiv.enabled=FALSE "
# all passwords below are set using the otxecm-secret
# Below is a base64 encoding of 123456789abcdefg
CMDLINE+=" --set otds.otdsws.cryptKey='${OTDSCRYPTKEY}'"

# Database params
CMDLINE+=" --set otcs.config.database.hostname=otxecm-db "
CMDLINE+=" --set otds.otdsws.otdsdb.automaticDatabaseCreation.enabled=true "
CMDLINE+=" --set otds.otdsws.otdsdb.username=postgres "
CMDLINE+=" --set otds.otdsws.otdsdb.automaticDatabaseCreaion.dbName=otdsdb "
# FIrst attempt at pushing EMLC module leaving commented out for now
CMDLINE+=" --set otcs.config.extensions.enabled=true"
CMDLINE+=" --set otcs.initContainers[0].name=ccmemlc"
CMDLINE+=" --set otcs.initContainers[0].image.source=gcr.iou"
CMDLINE+=" --set otcs.initContainers[0].image.name=ccmemlc"
CMDLINE+=" --set otcs.initContainers[0].image.tag=23.4"
echo FILE ARGS:
echo $FILEARGS
echo COMMAND LINE ARGS:
echo $CMDLINE 


echo EXECUTING HELM....
echo "helm install ${DEPLNAME} ${WORKDIR}/${DEPLDIR} ${FILEARGS} ${CMDLINE}"
helm install ${DEPLNAME} ${WORKDIR}/${DEPLDIR} ${FILEARGS} ${CMDLINE}  
